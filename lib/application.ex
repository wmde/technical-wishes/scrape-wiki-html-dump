defmodule ScrapeWikiDump.Application do
  use Application

  import Telemetry.Metrics
  require Logger

  @impl true
  def start(_type, _args) do
    # FIXME: tags/labels are important but they currently break the reporter.
    metrics = [
      distribution(
        "scraper_analyze_plugin_duration",
        event_name: ~w(scraper analyze stop)a,
        measurement: :duration,
        reporter_options: [
          buckets: [0.0001, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10]
        ],
        tags: [:plugin],
        unit: {:native, :second}
      ),
      distribution(
        "scraper_mapdata_fetch_duration",
        event_name: ~w(scraper fetch_mapdata stop)a,
        measurement: :duration,
        reporter_options: [
          buckets: [0.01, 0.05, 0.1, 0.25, 0.5, 1, 5, 10]
        ],
        # tags: [:wiki],
        unit: {:native, :second}
      ),
      distribution(
        "scraper_process_wiki_duration",
        event_name: ~w(scraper process_wiki stop)a,
        measurement: :duration,
        reporter_options: [
          buckets: [1, 10, 100, 1_000, 10_000, 100_000, 1_000_000]
        ],
        tags: [:wiki],
        unit: {:native, :second}
      ),
      distribution(
        "scraper_summarize_page_duration",
        event_name: ~w(scraper summarize_page stop)a,
        measurement: :duration,
        reporter_options: [
          buckets: [0.0001, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10]
        ],
        tags: [:wiki],
        unit: {:native, :second}
      ),
      last_value("vm.memory.total", unit: :byte),
      last_value("vm.system_counts.process_count"),
      last_value("vm.total_run_queue_lengths.total")
    ]

    children = [
      {TelemetryMetricsPrometheus,
       [
         metrics: metrics,
         port: Application.get_env(:scrape_wiki_dump, :prometheus_metrics_port)
       ]}
      # {Telemetry.Metrics.ConsoleReporter, metrics: metrics}
    ]

    Logger.add_handlers(:scrape_wiki_dump)
    Logger.info("Starting scraper with version #{Application.spec(:scrape_wiki_dump, :vsn)}")

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
