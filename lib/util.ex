defmodule Wiki.DumpUtils do
  @moduledoc false

  @doc """
  Merge two maps, adding the values for overlapping keys.
  """
  def sum_maps(map1, map2) do
    Map.merge(
      map1,
      map2,
      fn _, v1, v2 when is_integer(v1) and is_integer(v2) -> v1 + v2 end
    )
  end

  def ratio(_, 0), do: 0

  def ratio(denominator, total) do
    (denominator / total)
    |> Float.round(4)
  end

  def stream_binary_file(path) when is_binary(path),
    do: path |> File.stream!([read_ahead: 100_000], 10_000)

  @spec decode_ndjson_tar_gz(Enumerable.t(binary())) :: Enumerable.t(map())
  @doc """
  Read a tar'ed and gzip'ed stream of json records.
  """
  def decode_ndjson_tar_gz(stream) do
    stream
    |> TarCat.stream!()
    |> SplitLines.stream!()
    |> decode_ndjson()
  end

  @doc """
  Serialize a stream of entities each as a line of json.
  """
  def encode_ndjson(stream) do
    stream
    |> Stream.map(fn row -> [JiffyEx.encode!(row), "\n"] end)
  end

  @doc """
  Compress a file using an external command.
  """
  def gzip(path) do
    System.cmd("gzip", ["--best", path], parallelism: true)
  end

  @spec decode_ndjson_gz(Enumerable.t(binary())) :: Enumerable.t(String.t())
  @doc """
  Read a gzipped stream of json lines, decode each into a stream of entities.
  """
  def decode_ndjson_gz(stream) do
    stream
    |> StreamGzip.gunzip()
    |> SplitLines.stream!()
    |> decode_ndjson()
  end

  @spec decode_ndjson(Enumerable.t(String.t())) :: Enumerable.t(map())
  @doc """
  Read a stream of json lines, decode into a stream of Elixir Map.
  """
  def decode_ndjson(lines) do
    lines
    |> Stream.filter(&(&1 != ""))
    |> Stream.map(&JiffyEx.decode!(&1, return_maps: true))
  end
end
