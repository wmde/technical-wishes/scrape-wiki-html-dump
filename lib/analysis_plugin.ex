defmodule Wiki.DumpsAnalysis.Plugin do
  @moduledoc """
  Behavior implemented by an analysis plugin.

  This allows analysis to be split up into independent modules which are only
  responsible for one domain of data.
  """

  defmodule AggregationColumn do
    @enforce_keys [:column]
    defstruct [:column, :initial_value, :aggregator, :finalizer, :avg]

    @type t :: %__MODULE__{
            column: atom(),
            initial_value: any(),
            aggregator: (map(), any() -> any()),
            finalizer: (map() -> any()),
            avg: boolean()
          }
  end

  @doc """
  Process a page and return it with additional fields.
  """
  @callback analyze_page(map()) :: map()

  @doc """
  Return data structures holding the aggregation logic.

  Only the columns returned in `.column` will be included in the per-wiki output.

  TODO: Make less OO
  """
  @callback aggregations() :: list(AggregationColumn.t())

  defmacro __using__(_opts) do
    quote do
      @behaviour Wiki.DumpsAnalysis.Plugin

      import Wiki.DumpsAnalysis.Plugin
    end
  end

  @spec aggregate(atom(), any(), (map(), any() -> any()), list()) ::
          AggregationColumn.t()
  def aggregate(column, initial_value, aggregator, opts \\ []) do
    %AggregationColumn{
      column: column,
      initial_value: initial_value,
      aggregator: aggregator
    }
    |> Map.merge(Map.new(opts))
  end

  @spec aggregate_final(atom(), (map() -> any()), list()) ::
          AggregationColumn.t()
  def aggregate_final(column, finalizer, opts \\ []) do
    %AggregationColumn{
      column: column,
      finalizer: finalizer
    }
    |> Map.merge(Map.new(opts))
  end
end
