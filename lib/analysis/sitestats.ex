defmodule Wiki.DumpsAnalysis.SiteStats do
  use Wiki.DumpsAnalysis.Plugin
  require Logger

  @impl true
  def analyze_page(_), do: %{}

  @spec fetch_articles_from_response(map()) :: {:ok, integer()} | {:error, atom()}
  defp fetch_articles_from_response(%{
         result: %{"query" => %{"statistics" => %{"articles" => number}}}
       }) do
    {:ok, number}
  end

  defp fetch_articles_from_response(_), do: {:error, :reason}

  @spec get_number_of_articles(Wiki.SiteMatrix.Spec.t()) :: integer()
  def get_number_of_articles(site) do
    with {:ok, response} <- fetch_statistics(site),
         {:ok, number} <- fetch_articles_from_response(response) do
      number
    else
      {:error, reason} ->
        Logger.warning("Error fetching siteinfo from #{site.dbname}: #{inspect(reason)}")
        -1
    end
  end

  @spec fetch_site(String.t()) :: Wiki.SiteMatrix.Spec.t()
  defp fetch_site(wiki) do
    Wiki.SiteMatrix.new()
    |> Wiki.SiteMatrix.get!(wiki)
  end

  defp fetch_statistics(site) do
    Wiki.Action.new(site)
    |> Wiki.Action.get(
      action: :query,
      meta: :siteinfo,
      siprop: :statistics
    )
  end

  @impl true
  def aggregations do
    [
      aggregate(
        :article_count_stats,
        0,
        fn _row, acc -> acc end,
        finalizer: fn totals ->
          fetch_site(totals.wiki)
          |> get_number_of_articles()
        end
      )
    ]
  end
end
