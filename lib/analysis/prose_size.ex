defmodule Wiki.DumpsAnalysis.ProseSize do
  use Wiki.DumpsAnalysis.Plugin

  @impl true
  @doc """
  Calculate "prose" size by stripping the article down to just the body content
  without markup and measuring text statistics.

  For background about prose size, see
  * https://en.wikipedia.org/wiki/Wikipedia:Prosesize

  This algorithm is meant to closely match the existing implementations:
  * https://github.com/x-tools/xtools/blob/main/src/Model/ArticleInfoApi.php
  * https://github.com/wikimedia-gadgets/prosesize/blob/master/prosesize.js
  * https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/blob/main/wikipedia_prosesize/src/lib.rs
  """
  def analyze_page(%{dom: dom}) do
    rule = ~w(
      style
      #coordinates
      [class*="emplate"]
      [typeof~="mw:Extension/math"]
      [typeof~="mw:Extension/ref"]
    ) |> Enum.join(",")

    text =
      dom
      |> Floki.filter_out(rule)
      |> Floki.find("section > p")
      |> Enum.map_join(" ", &Floki.text(&1, sep: " "))

    %{
      prose_size: text |> String.length(),
      word_count: Regex.scan(~r/[[:space:]]+/su, text, return: :index) |> length()
    }
  end

  @impl true
  def aggregations do
    [
      aggregate(:prose_size_average, 0, fn row, acc -> row["prose_size"] + acc end,
        avg: :page_count
      ),
      aggregate(:word_count_average, 0, fn row, acc -> row["word_count"] + acc end,
        avg: :page_count
      )
    ]
  end
end
