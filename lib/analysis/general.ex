defmodule Wiki.DumpsAnalysis.General do
  use Wiki.DumpsAnalysis.Plugin

  @impl true
  def analyze_page(%{raw_line: line, site: site}) do
    %{
      # FIXME: Don't hide the dependencies between analysis plugins, distinguish
      # between inputs, private and shared intermediate values, and outputs.
      dom: parse_html(line["article_body"]["html"] || ""),
      pageid: line["identifier"],
      revid: line["version"]["identifier"],
      title: line["name"],
      wiki: site.dbname,
      html_length: byte_size(line["article_body"]["html"] || ""),
      wikitext_length: byte_size(line["article_body"]["wikitext"] || "")
    }
  end

  defp parse_html(html) when is_binary(html),
    do: Floki.parse_document!(html, html_parser: Floki.HTMLParser.FastHtml)

  @impl true
  def aggregations do
    [
      aggregate(:wiki, 0, fn row, _acc -> row["wiki"] end),
      aggregate(:page_count, 0, fn _row, acc -> acc + 1 end),
      aggregate(:wikitext_length_average, 0, fn row, acc -> row["wikitext_length"] + acc end,
        avg: :page_count
      )
    ]
  end
end
