defmodule Wiki.DumpsAnalysis.KartographerMaps do
  use Wiki.DumpsAnalysis.Plugin

  import Wiki.Mapdata, only: [fetch_and_summarize_mapdata: 2]

  @impl true
  def analyze_page(%{dom: dom, revid: revid, site: site}) do
    mapframes = find_mapframes(dom)
    maplinks = find_maplinks(dom)

    %{
      mapframe_count: length(mapframes),
      mapframe_from_transclusion_count: Enum.count(mapframes, &transclusion?/1),
      maplink_count: length(maplinks),
      maplink_from_transclusion_count: Enum.count(maplinks, &transclusion?/1)
    }
    |> optional_fetch_mapdata(site, revid)
  end

  defp find_mapframes(node), do: Floki.find(node, ~s([typeof~="mw:Extension/mapframe"]))

  defp find_maplinks(node), do: Floki.find(node, ~s([typeof~="mw:Extension/maplink"]))

  defp transclusion?(node) do
    node
    |> Floki.attribute("typeof")
    |> Enum.join(" ")
    |> String.contains?("mw:Transclusion")
  end

  @spec optional_fetch_mapdata(map(), Wiki.SiteMatrix.Spec.t(), integer()) :: map()
  defp optional_fetch_mapdata(%{mapframe_count: 0, maplink_count: 0} = summary, _, _), do: summary

  defp optional_fetch_mapdata(summary, site, revid) do
    Map.merge(summary, fetch_and_summarize_mapdata(site, revid))
  end

  @externaldata_types ~w(
    geoline_ids
    geoline_query
    geomask_ids
    geomask_query
    geopoint_ids
    geopoint_query
    geoshape_ids
    geoshape_query
    page
  )

  @impl true
  def aggregations do
    [
      aggregate(:mapframe_sum, 0, fn row, acc -> row["mapframe_count"] + acc end),
      aggregate(:mapframe_from_transclusion_sum, 0, fn row, acc ->
        row["mapframe_from_transclusion_count"] + acc
      end),
      aggregate(:maplink_sum, 0, fn row, acc -> row["maplink_count"] + acc end),
      aggregate(:maplink_from_transclusion_sum, 0, fn row, acc ->
        row["maplink_from_transclusion_count"] + acc
      end),
      aggregate(:pages_with_maps_count, 0, fn
        %{"mapframe_count" => 0, "maplink_count" => 0}, acc -> acc
        _, acc -> 1 + acc
      end)
    ] ++
      (@externaldata_types
       |> Enum.map(fn type ->
         aggregate(String.to_atom("externaldata_#{type}"), 0, fn
           %{"externaldata" => %{^type => count}}, acc -> count + acc
           _, acc -> acc
         end)
       end))
  end
end
