defmodule Wiki.DumpsAnalysis.CiteRefs do
  use Wiki.DumpsAnalysis.Plugin

  import Wiki.DumpUtils, only: [sum_maps: 2]

  @edit_distance_fraction 0.10

  @impl true
  def analyze_page(%{dom: dom}) do
    refs = find_refs(dom)
    top_level_transclusion_refs = Enum.filter(refs, &transclusion_type?/1)

    reflists = find_reflists(dom)
    transclusions = find_full_transclusions(dom)

    transclusions_with_contained_refs =
      transclusions
      |> Enum.map(&{&1, find_refs(&1)})
      |> Enum.filter(&(elem(&1, 1) != []))

    ref_bodies =
      reflists
      |> Enum.map(&find_ref_bodies/1)
      |> Enum.concat()

    nested_refs =
      find_refs(ref_bodies)

    ref_solely_transclusion_bodies =
      ref_bodies
      |> Enum.filter(&solely_transclusion_node?/1)

    ref_body_texts =
      ref_bodies
      |> Enum.map(&Floki.text/1)
      |> Enum.reject(&(&1 == ""))

    ref_body_similarities =
      ref_body_texts
      |> compare_texts()
      |> Enum.frequencies()

    ref_mw_data = Enum.map(refs, &parse_mw/1)
    named_refs = Enum.filter(ref_mw_data, &named?/1)

    # Make sure we dont count refs twice for the case that missing mwdata for these refs is fixed
    top_level_transclusion_refs_names =
      Enum.filter(top_level_transclusion_refs, &ref_without_name_in_mwdata?/1)
      |> Enum.map(&get_group_and_name_from_ref/1)
      |> Enum.filter(&elem(&1, 1))

    counts_by_name =
      named_refs
      |> Enum.map(&{&1["attrs"]["group"], &1["attrs"]["name"]})
      |> Enum.concat(top_level_transclusion_refs_names)
      |> Enum.frequencies()

    automatic_refs_names_with_count =
      counts_by_name
      |> Map.filter(fn {{_group, name}, _count} -> String.match?(name, ~r/^:\d+$/) end)

    %{
      automatic_ref_names_count: map_size(automatic_refs_names_with_count),
      automatic_ref_name_usages_count:
        automatic_refs_names_with_count
        |> Map.values()
        |> Enum.sum(),
      identical_ref_count: ref_body_similarities[:identical] || 0,
      list_defined_ref_count: count_list_defined_refs(reflists),
      nested_ref_count: length(nested_refs),
      potential_ref_transclusions:
        transclusions_with_contained_refs
        |> Enum.map(fn {nodes, _} ->
          Enum.filter(nodes, &transclusion_type?/1)
        end)
        |> Enum.map(&parse_mw/1)
        |> Enum.flat_map(&template_names/1)
        |> Enum.frequencies(),
      potential_transclusions_with_top_level_refs:
        top_level_transclusion_refs
        |> Enum.map(&parse_mw/1)
        |> Enum.flat_map(&template_names/1)
        |> Enum.frequencies(),
      ref_by_transclusion_count:
        transclusions_with_contained_refs
        |> Enum.map(&length(elem(&1, 1)))
        |> Enum.sum(),
      ref_by_top_ref_transclusion_count: length(top_level_transclusion_refs),
      ref_count: length(refs),
      ref_error_counts_by_type: count_error_frequency(ref_mw_data),
      ref_reuse_counts:
        counts_by_name
        |> Map.values()
        |> Enum.map(&(&1 - 1))
        |> Enum.filter(&(&1 > 0))
        |> Enum.sort()
        |> Enum.reverse(),
      ref_with_name_count:
        counts_by_name
        |> Map.values()
        |> Enum.sum(),
      reflist_count: length(reflists),
      refs_with_solely_transclusion_count: length(ref_solely_transclusion_bodies),
      refs_with_transclusions_count: Enum.count(ref_bodies, &(find_transclusions(&1) != [])),
      similar_ref_count: ref_body_similarities[:similar] || 0,
      transclusion_count: length(transclusions),
      transclusions_inside_refs:
        ref_bodies
        |> Enum.flat_map(&find_transclusions/1)
        |> Enum.map(&parse_mw/1)
        |> Enum.flat_map(&template_names/1)
        |> Enum.frequencies(),
      unique_name_count: map_size(counts_by_name)
    }
  end

  defp ref_without_name_in_mwdata?(node) do
    not (parse_mw(node) |> named?())
  end

  defp get_group_and_name_from_ref(node) do
    name =
      Floki.attribute(node, "id")
      |> Enum.filter(&String.match?(&1, ~r/cite_ref-.*?_/))
      |> Enum.map(&String.replace(&1, ~r/^cite_ref-(.*?)(_.*)/, "\\1"))
      |> List.first()

    group =
      Floki.find(node, "a")
      |> Floki.attribute("data-mw-group")
      |> List.first()

    {group, name}
  end

  defp find_ref_bodies(node), do: Floki.find(node, ~s(span.mw-reference-text))

  defp solely_transclusion_node?(node) do
    Floki.children(node)
    |> case do
      [child | rest] when is_tuple(child) ->
        transclusion_type?(child) &&
          Enum.all?(rest, fn sibling ->
            is_tuple(sibling) &&
              Floki.attribute(sibling, "about") == Floki.attribute(child, "about")
          end)

      _ ->
        false
    end
  end

  defp find_reflists(node), do: Floki.find(node, ~s([typeof~="mw:Extension/references"]))

  defp find_refs(node), do: Floki.find(node, ~s(sup[typeof~="mw:Extension/ref"]))

  defp find_transclusions(node), do: Floki.find(node, ~s([typeof~="mw:Transclusion"]))

  defp find_full_transclusions(node) do
    Floki.find(node, ~s([typeof~="mw:Transclusion"]))
    |> Enum.map(fn first_node ->
      [about_id] = Floki.attribute(first_node, "about")
      Floki.find(node, ~s([about="#{about_id}"]))
    end)
  end

  defp count_list_defined_refs(reflists) do
    reflists
    |> Enum.map(&find_list_defined_refs/1)
    |> Enum.concat()
    |> Enum.count()
  end

  defp find_list_defined_refs(node) do
    mw_data = parse_mw(node)

    case mw_data["body"]["html"] do
      nil ->
        []

      content ->
        content
        |> Floki.parse_fragment!()
        |> find_refs()
    end
  end

  @spec count_error_frequency(list(map())) :: map()
  defp count_error_frequency(ref_mw_data) do
    ref_mw_data
    |> Enum.flat_map(fn
      %{"errors" => errors} -> Enum.map(errors, & &1["key"])
      _ -> []
    end)
    |> Enum.frequencies()
  end

  # Only parse the first part of a template
  defp parse_mw([node | _]), do: parse_mw(node)

  defp parse_mw(node) do
    node
    |> Floki.attribute("data-mw")
    |> JiffyEx.decode!(return_maps: true)
  end

  defp template_names(mw) do
    mw["parts"]
    |> Enum.map(fn
      part when is_map(part) -> part["template"]["target"]["wt"]
      _ -> nil
    end)
    |> Enum.filter(& &1)
    |> Enum.map(&normalize_template_name/1)
  end

  @doc false
  def normalize_template_name(name) do
    name = Regex.replace(~r/<!--.*?-->/s, name, "")
    name = Regex.replace(~r/[\s_]+/, name, " ")

    {first, rest} =
      name
      |> String.trim()
      |> String.split_at(1)

    # TODO: This is incorrect for wiktionaries (and jbo, pwn, szy, and taywiki)
    # because of the $wgCapitalLinks configuration.  Practically, it means that
    # on those wikis we may see templates with differing initial letter case get
    # merged when they shouldn't be.
    String.upcase(first) <> rest
  end

  defp transclusion_type?(node) do
    Floki.attribute(node, "typeof")
    |> case do
      [] -> false
      [typeof_value] -> String.contains?(typeof_value, "mw:Transclusion")
    end
  end

  defp named?(mw), do: Map.has_key?(mw, "attrs") and Map.has_key?(mw["attrs"], "name")

  @doc false
  def compare_texts(texts)

  def compare_texts([]), do: []

  def compare_texts([ref | rest]) do
    compare_text_with_list(ref, rest) ++ compare_texts(rest)
  end

  defp compare_text_with_list(ref, rest) do
    Enum.find_value(rest, [], &compare_pair(ref, &1))
  end

  defp compare_pair(a, b) do
    len_a = String.length(a)
    len_b = String.length(b)
    length_difference = abs(len_a - len_b)
    longest_length = max(len_a, len_b)

    cond do
      length_difference == 0 && a == b -> [:identical]
      length_difference > 10 -> false
      Levenshtein.distance(a, b) < longest_length * @edit_distance_fraction -> [:similar]
      true -> false
    end
  end

  @impl true
  def aggregations do
    [
      aggregate(:identical_refs_count, 0, fn row, acc -> row["identical_ref_count"] + acc end),
      aggregate(:list_defined_ref_sum, 0, fn row, acc -> row["list_defined_ref_count"] + acc end),
      aggregate(:nested_ref_sum, 0, fn row, acc -> row["nested_ref_count"] + acc end),
      aggregate(
        :max_ref_reuse_average,
        0,
        fn
          %{"ref_reuse_counts" => []}, acc -> acc
          %{"ref_reuse_counts" => counts}, acc -> hd(counts) + acc
        end,
        avg: :pages_with_ref_reuse_count
      ),
      aggregate(:pages_with_automatically_named_refs_count, 0, fn
        %{"automatic_ref_names_count" => 0}, acc -> acc
        _, acc -> 1 + acc
      end),
      aggregate(:pages_with_identical_refs_count, 0, fn
        %{"identical_ref_count" => 0}, acc -> acc
        _, acc -> 1 + acc
      end),
      aggregate(:pages_with_multiple_reflists_count, 0, fn
        %{"reflist_count" => count}, acc when count < 2 -> acc
        _, acc -> 1 + acc
      end),
      aggregate(:pages_with_named_refs_count, 0, fn
        %{"ref_with_name_count" => 0}, acc -> acc
        _, acc -> 1 + acc
      end),
      aggregate(:pages_with_nested_refs_count, 0, fn row, acc ->
        case row["nested_ref_count"] do
          0 -> 0
          _ -> 1
        end + acc
      end),
      aggregate(:pages_with_ref_reuse_count, 0, fn
        %{"ref_reuse_counts" => []}, acc -> acc
        _, acc -> 1 + acc
      end),
      aggregate(:pages_with_refs_count, 0, fn row, acc ->
        case row["ref_count"] do
          0 -> 0
          _ -> 1
        end + acc
      end),
      aggregate(:pages_with_similar_refs_count, 0, fn row, acc ->
        case row["similar_ref_count"] do
          0 -> 0
          _ -> 1
        end + acc
      end),
      aggregate(
        :potential_ref_transclusions,
        %{},
        fn row, acc ->
          sum_maps(
            row["potential_ref_transclusions"],
            acc
          )
        end,
        finalizer: fn totals ->
          totals.potential_ref_transclusions
          |> Map.to_list()
          |> Enum.sort_by(&elem(&1, 1), :desc)
          |> Enum.map(fn {key, value} -> [key, value] end)
        end
      ),
      aggregate(
        :potential_transclusions_with_top_level_refs,
        %{},
        fn row, acc ->
          sum_maps(
            row["potential_transclusions_with_top_level_refs"],
            acc
          )
        end,
        finalizer: fn totals ->
          totals.potential_transclusions_with_top_level_refs
          |> Map.to_list()
          |> Enum.sort_by(&elem(&1, 1), :desc)
          |> Enum.map(fn {key, value} -> [key, value] end)
        end
      ),
      aggregate(
        :proportion_of_named_refs_uniquely_named_average,
        0,
        fn row, acc ->
          case row["ref_with_name_count"] do
            0 -> 0
            c -> row["unique_name_count"] / c
          end + acc
        end,
        avg: :pages_with_named_refs_count
      ),
      aggregate(
        :proportion_of_refs_named_average,
        0,
        fn row, acc ->
          case row["ref_count"] do
            0 -> 0
            c -> row["ref_with_name_count"] / c
          end + acc
        end,
        avg: :pages_with_refs_count
      ),
      aggregate(
        :proportion_of_refs_reused_average,
        0,
        fn row, acc ->
          case row["ref_count"] do
            0 -> 0
            c -> (row["ref_with_name_count"] - row["unique_name_count"]) / c
          end + acc
        end,
        avg: :pages_with_refs_count
      ),
      aggregate(:ref_by_transclusion_count, 0, fn row, acc ->
        row["ref_by_transclusion_count"] + acc
      end),
      aggregate(:ref_count, 0, fn row, acc -> row["ref_count"] + acc end),
      aggregate(
        :ref_error_counts_by_type,
        %{},
        fn row, acc ->
          sum_maps(
            row["ref_error_counts_by_type"],
            acc
          )
        end
      ),
      aggregate(:reflist_count, 0, fn row, acc ->
        row["reflist_count"] + acc
      end),
      aggregate(:refs_with_solely_transclusion_count, 0, fn row, acc ->
        row["refs_with_solely_transclusion_count"] + acc
      end),
      aggregate(:refs_with_transclusions_count, 0, fn row, acc ->
        row["refs_with_transclusions_count"] + acc
      end),
      aggregate(:similar_refs_count, 0, fn row, acc ->
        row["similar_ref_count"] + acc
      end),
      aggregate(:transclusion_sum, 0, fn row, acc ->
        row["transclusion_count"] + acc
      end),
      aggregate(
        :transclusions_inside_refs,
        %{},
        fn row, acc -> sum_maps(row["transclusions_inside_refs"], acc) end,
        finalizer: fn totals ->
          totals.transclusions_inside_refs
          |> Map.to_list()
          |> Enum.sort_by(&elem(&1, 1), :desc)
          |> Enum.map(fn {key, value} -> [key, value] end)
        end
      ),
      aggregate(:pages_with_over_25_refs_count, 0, fn
        %{"ref_count" => ref_count}, acc when ref_count > 25 -> 1 + acc
        _, acc -> acc
      end),
      aggregate(:pages_with_identical_refs_and_over_25_refs_count, 0, fn
        %{"identical_ref_count" => 0}, acc -> acc
        %{"ref_count" => ref_count}, acc when ref_count <= 25 -> acc
        _, acc -> 1 + acc
      end),
      aggregate(
        :identical_refs_on_pages_with_over_25_refs_count,
        0,
        fn
          %{"ref_count" => ref_count} = row, acc when ref_count > 25 ->
            row["identical_ref_count"] + acc

          _, acc ->
            acc
        end
      ),
      aggregate_final(
        :identical_refs_on_pages_with_over_25_refs_average,
        & &1.identical_refs_on_pages_with_over_25_refs_count,
        avg: :pages_with_over_25_refs_count
      ),
      aggregate_final(
        :identical_refs_on_pages_with_25_or_less_refs_average,
        &Wiki.DumpUtils.ratio(
          &1.identical_refs_count - &1.identical_refs_on_pages_with_over_25_refs_count,
          &1.pages_with_refs_count - &1.pages_with_over_25_refs_count
        )
      ),
      aggregate_final(
        :list_defined_ref_per_page_having_ref,
        & &1.list_defined_ref_sum,
        avg: :pages_with_refs_count
      ),
      aggregate_final(
        :proportion_of_pages_with_identical_refs,
        & &1.pages_with_identical_refs_count,
        avg: :pages_with_refs_count
      ),
      aggregate_final(
        :proportion_of_pages_with_nested_refs,
        & &1.pages_with_nested_refs_count,
        avg: :pages_with_refs_count
      ),
      aggregate_final(
        :proportion_of_pages_with_similar_refs,
        & &1.pages_with_similar_refs_count,
        avg: :pages_with_refs_count
      ),
      aggregate_final(
        :proportion_of_pages_with_refs,
        & &1.pages_with_refs_count,
        avg: :page_count
      ),
      aggregate_final(
        :proportion_of_refs_from_transclusion,
        & &1.ref_by_transclusion_count,
        avg: :ref_count
      ),
      aggregate_final(
        :proportion_of_refs_having_transclusion,
        & &1.refs_with_transclusions_count,
        avg: :ref_count
      ),
      aggregate_final(
        :ref_by_transclusion_average,
        & &1.ref_by_transclusion_count,
        avg: :pages_with_refs_count
      ),
      aggregate_final(
        :ref_count_per_page,
        & &1.ref_count,
        avg: :page_count
      ),
      aggregate_final(
        :ref_count_per_page_having_ref,
        & &1.ref_count,
        avg: :pages_with_refs_count
      ),
      aggregate_final(
        :ref_error_average,
        fn totals ->
          totals.ref_error_counts_by_type |> Map.values() |> Enum.sum()
        end,
        avg: :pages_with_refs_count
      ),
      aggregate_final(
        :reflists_per_page_having_ref,
        & &1.reflist_count,
        avg: :pages_with_refs_count
      ),
      aggregate_final(
        :transclusion_average,
        & &1.transclusion_sum,
        avg: :page_count
      )
    ]
  end
end
