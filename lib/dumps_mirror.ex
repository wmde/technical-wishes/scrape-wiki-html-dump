defmodule Wiki.DumpsMirror do
  @moduledoc """
  Read HTML dumps from a filesystem mirror, such as an NFS mount on Wikimedia
  Cloud VPS.

  When dumps are available on the filesystem, configure the path and stable
  snapshot in `config/prod.exs`.
  """
  def get_root_dir, do: Application.get_env(:scrape_wiki_dump, :dumps_root)

  def get_snapshot_dir(snapshot) do
    Path.join(get_root_dir(), snapshot)
  end

  def find_all_available_wikis(snapshot) do
    snapshot
    |> get_snapshot_dir()
    |> File.ls!()
    |> dump_paths_to_wikis()
  end

  def dump_paths_to_wikis(paths) do
    paths
    |> Enum.map(&dump_path_to_wiki/1)
    |> Enum.filter(& &1)
  end

  def dump_path_to_wiki(filename) do
    Regex.run(~r/(\w+)-NS0-\d+-ENTERPRISE-HTML.json.tar.gz/, filename)
    |> case do
      nil -> nil
      [_, wiki] -> wiki
    end
  end

  def find_dump(name, snapshot) do
    root_dir = get_root_dir()

    filename = "#{name}-NS0-#{snapshot}-ENTERPRISE-HTML.json.tar.gz"
    Path.join([root_dir, snapshot, filename])
  end

  def find_latest_snapshot(root_dir) do
    File.ls!(root_dir)
    |> Enum.sort()
    |> Enum.reverse()
    |> hd
  end
end
