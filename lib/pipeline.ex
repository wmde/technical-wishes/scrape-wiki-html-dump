defmodule Wiki.DumpPipeline do
  @moduledoc """
  Wire streams between files and processes.

  Skip completed output files and continue partial files.
  """
  alias Wiki.Checkpointer
  alias Wiki.DumpUtils
  alias Wiki.SummaryAggregator
  require Logger

  @type analysis_stage() :: :page_summary | :wiki_summary | :aggregate_all

  @doc """
  Run specified analysis stages for the given sites.
  """
  @spec run_pipeline(list(Wiki.SiteMatrix.Spec.t()), String.t(), Keyword.t()) :: any()
  def run_pipeline(sites, snapshot, opts \\ []) do
    stages =
      case opts[:skip_page_summary] do
        true -> []
        _ -> [:page_summary]
      end ++ [:wiki_summary, :aggregate_all]

    sites = validate_wikis(sites, stages, snapshot, opts)

    File.mkdir_p!(report_root())

    # TODO: This is irrevocable, assumes only one use case.
    Logger.configure_backend(:console, device: Owl.LiveScreen)

    Owl.ProgressBar.start(
      id: :sites,
      label: "Overall progress",
      total: length(sites),
      absolute_values: true
    )

    if :page_summary in stages do
      sites
      |> Enum.each(&run_isolated_wiki_summary(&1, snapshot))
    end

    if :wiki_summary in stages do
      sites
      |> Enum.each(&aggregate_wiki(&1, snapshot))
    end

    if :aggregate_all in stages do
      aggregate_across_wikis(sites, snapshot)
    end
  end

  defp validate_wikis(wikis, stages, snapshot, opts) do
    # Ensure that all wikis to process are represented as input files and are
    # known from the site matrix.

    {:ok, sites} =
      Wiki.SiteMatrix.new(opts)
      |> Wiki.SiteMatrix.get_all()

    sites = Enum.reject(sites, fn site -> site.private end)

    available_dumps =
      cond do
        :page_summary in stages ->
          Wiki.DumpsMirror.find_all_available_wikis(snapshot)

        :wiki_summary in stages ->
          sites
          |> Enum.filter(fn site ->
            page_summary_compressed_path(site.dbname, snapshot)
            |> File.exists?()
          end)
          |> Enum.map(& &1.dbname)

        :aggregate_all in stages ->
          sites
          |> Enum.filter(fn site ->
            wiki_summary_path(site.dbname, snapshot)
            |> File.exists?()
          end)
          |> Enum.map(& &1.dbname)
      end

    case wikis do
      [] ->
        sites

      requested ->
        known = Enum.map(sites, & &1.dbname)

        # Crash if any invalid wikis were given on the commandline.
        Enum.each(requested, fn wiki ->
          if wiki not in known do
            raise "Unknown wiki #{wiki}"
          end
        end)

        Enum.filter(sites, fn site ->
          site.dbname in requested
        end)
    end
    |> Enum.filter(fn site ->
      if site.dbname in available_dumps do
        true
      else
        Logger.warning("No source available for #{site.dbname}")
        false
      end
    end)
    |> Enum.sort_by(& &1.dbname)
  end

  defp run_isolated_wiki_summary(site, snapshot) do
    # TODO: Refactor to be more Elixiry
    measured_wiki_summary(site, snapshot)
    :ok
  rescue
    err ->
      Logger.error(wiki: site.dbname, error: err)
      {:error, err}
  end

  defp measured_wiki_summary(site, snapshot) do
    :telemetry.span(
      ~w(scraper process_wiki)a,
      %{},
      fn ->
        {
          full_wiki_summary(site, snapshot),
          %{wiki: site.dbname}
        }
      end
    )
  end

  defp full_wiki_summary(site, snapshot) do
    summarize_pages(site, snapshot)

    Owl.ProgressBar.inc(id: :sites)
  end

  defp configured_modules,
    do: Application.get_env(:scrape_wiki_dump, :analysis)

  # Read summary data for the given wikis and create a summary CSV.
  defp aggregate_across_wikis(sites, snapshot) do
    all_wiki_data =
      sites
      |> Enum.map(fn %{dbname: wiki} ->
        wiki_summary_path(wiki, snapshot)
        |> File.read!()
        |> JiffyEx.decode!(return_maps: true)
      end)

    standard_columns_csv =
      all_wiki_data
      |> Enum.map(fn wiki_data ->
        Map.filter(wiki_data, fn {_key, value} -> is_number(value) or is_binary(value) end)
      end)
      |> CSV.encode(headers: SummaryAggregator.aggregate_columns())
      |> Enum.join()

    File.write(grand_summary_path(snapshot), standard_columns_csv)

    aggregate_errors(all_wiki_data, snapshot)
  end

  # TODO: Belongs in the cite_refs analysis module, or more generalized
  def aggregate_errors(all_wiki_data, snapshot) do
    error_keys =
      all_wiki_data
      |> Enum.flat_map(fn wiki_data ->
        wiki_data
        |> Map.get("ref_error_counts_by_type", %{})
        |> Map.keys()
      end)
      |> Enum.uniq()
      |> Enum.sort()

    zero_pads = Map.from_keys(error_keys, 0)

    padded_flattened_data =
      all_wiki_data
      |> Enum.map(fn wiki_data ->
        wiki_data
        |> Map.take(["wiki"])
        |> Map.merge(zero_pads)
        |> Map.merge(wiki_data["ref_error_counts_by_type"] || %{})
      end)

    csv_output =
      padded_flattened_data
      |> CSV.encode(headers: ["wiki"] ++ error_keys)
      |> Enum.join()

    File.write(cite_error_summary_path(snapshot), csv_output)
  end

  defp read_dump(wiki, snapshot) do
    # Note that this is production-only, there's no web alternative.
    path = Wiki.DumpsMirror.find_dump(wiki, snapshot)
    Logger.debug("Reading #{wiki} dump from #{path}")

    path
    |> DumpUtils.stream_binary_file()
    |> DumpUtils.decode_ndjson_tar_gz()
  end

  defp summarize_pages(%{dbname: wiki} = site, snapshot) do
    # TODO: Check output existence and completeness, using the checkpointer.
    output_path = page_summary_path(wiki, snapshot)

    Checkpointer.resumable_pipeline(
      output_path,
      fn -> read_dump(wiki, snapshot) end,
      fn stream ->
        Logger.debug("Writing page summaries for #{wiki} to #{output_path}")

        stream
        |> Task.async_stream(
          fn line -> measured_summarize_page(line, site) end,
          ordered: false,
          timeout: 5 * 60 * 1_000,
          on_timeout: :kill_task,
          zip_input_on_exit: true
        )
        |> Stream.map(fn
          {:ok, row} ->
            row

          {:exit, {line, _}} ->
            Logger.error("Timed out on #{wiki} revision #{line["version"]["identifier"]}")
            nil
        end)
        |> Stream.filter(& &1)
        |> DumpUtils.encode_ndjson()
      end
    )

    if File.exists?(output_path) do
      DumpUtils.gzip(output_path)
    end
  end

  defp measured_summarize_page(line, site) do
    :telemetry.span(
      ~w(scraper summarize_page)a,
      %{},
      fn ->
        {
          summarize_page(line, site),
          %{wiki: site.dbname}
        }
      end
    )
  end

  defp summarize_page(dump_line, site) do
    configured_modules()
    |> Enum.reduce(
      %{
        raw_line: dump_line,
        site: site
      },
      &measured_analyze_page/2
    )
    # TODO: generalize cleanup, eg. "analyze_finish".  In fact, this has the
    # same plugin structure as aggregation, it should be reusable and simpler.
    |> Map.drop(~w(
      dom
      raw_line
      site
    )a)
  end

  defp measured_analyze_page(plugin, acc) do
    :telemetry.span(
      ~w(scraper analyze)a,
      %{},
      fn ->
        {
          Map.merge(acc, plugin.analyze_page(acc)),
          %{plugin: plugin}
        }
      end
    )
  end

  defp read_summary(wiki, snapshot) do
    path = page_summary_compressed_path(wiki, snapshot)
    Logger.debug("Reading #{wiki} summary from #{path}")

    path
    |> DumpUtils.stream_binary_file()
    |> DumpUtils.decode_ndjson_gz()
  end

  defp aggregate_wiki(%{dbname: wiki}, snapshot) do
    output_path = wiki_summary_path(wiki, snapshot)

    Checkpointer.run_once(
      output_path,
      fn -> read_summary(wiki, snapshot) end,
      fn stream ->
        Logger.debug("Writing #{wiki} summary to #{output_path}")

        stream
        |> SummaryAggregator.aggregate_stream()
        |> Jason.encode!(pretty: true)
      end
    )
  end

  defp report_root, do: Application.get_env(:scrape_wiki_dump, :reports_root)

  defp build_path(wiki, snapshot, flavor), do: "#{report_root()}/#{wiki}-#{snapshot}-#{flavor}"

  defp grand_summary_path(snapshot), do: build_path("all-wikis", snapshot, "summary.csv")

  defp cite_error_summary_path(snapshot),
    do: build_path("all-wikis", snapshot, "cite-error-summary.csv")

  defp page_summary_path(wiki, snapshot), do: build_path(wiki, snapshot, "page-summary.ndjson")

  def page_summary_compressed_path(wiki, snapshot),
    do: build_path(wiki, snapshot, "page-summary.ndjson.gz")

  def wiki_summary_path(wiki, snapshot), do: build_path(wiki, snapshot, "summary.json")
end
