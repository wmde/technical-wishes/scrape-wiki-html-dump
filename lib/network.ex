defmodule Wiki.Network do
  @moduledoc """
  Helper methods for generic web access.
  """
  require Logger

  @doc """
  Read from an URL, streaming the output.
  """
  def stream_from_url(url) do
    Logger.debug("Streaming from #{url}")

    url
    |> HTTPStream.get(headers: [{"user-agent", user_agent()}])
    # FIXME: wrapping because of some incompatibility between HTTPStream and eg.
    # Exile's expected types
    |> Stream.chunk_every(1)
  end

  @doc """
  Fetch the entire contents into memory.
  """
  def read_from_url(url) do
    url |> stream_from_url() |> Enum.to_list() |> Enum.join()
  end

  defp user_agent, do: Application.get_env(:scrape_wiki_dump, :user_agent)
end
