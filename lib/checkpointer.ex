defmodule Wiki.Checkpointer do
  @moduledoc """
  Checkpoint a job and resume after failure by skipping.

  FIXME: Should prevent redundant jobs.
  """
  require Logger

  defmodule Opaque do
    @moduledoc false
    # Internal representation of checkpointer state.
    @type t :: %__MODULE__{
            checkpoint_path: binary(),
            counter: any(),
            interval: integer(),
            output_path: binary()
          }

    @enforce_keys ~w(counter interval output_path checkpoint_path)a
    defstruct @enforce_keys
  end

  @spec new(binary(), keyword()) :: Opaque.t()
  defp new(path, opts) do
    %Opaque{
      counter: :counters.new(1, []),
      interval: opts[:interval] || 100,
      output_path: path,
      checkpoint_path: checkpoint_path(path)
    }
  end

  @doc """
  Run the given processor, skipping some or all lines depending on the detected
  checkpoint and completed output file.

  A "resumable" pipeline is one where processing can be restarted after a chunk
  count given in a checkpoint file.  Note that other jobs might be resumable but
  "append" won't be an appropriate output style, aggregation for example could
  be made resumable but would require additional logic to save and read the
  checkpointed subtotal.  What's implemented here is "append when resuming".
  """
  @spec resumable_pipeline(
          binary(),
          (-> Enumerable.t()),
          (Enumerable.t() -> Enumerable.t()),
          keyword()
        ) ::
          any()
  def resumable_pipeline(output_path, reader, processor, opts \\ []) do
    c = new(output_path, opts)

    if File.exists?(c.checkpoint_path) and not File.exists?(output_path) do
      Logger.warning("Checkpoint but no output. Deleting stale checkpoint.")
      delete(c)
    end

    if complete?(c) do
      Logger.debug("Skipping over complete #{output_path}")
      nil
    else
      if not File.exists?(c.checkpoint_path) do
        do_save(c.checkpoint_path, 0)
      end

      if not File.exists?(output_path) do
        File.touch!(output_path)
      end

      report_progress(
        opts[:label] || output_path,
        fn ->
          reader.()
          |> skip(c)
          |> processor.()
          |> Stream.chunk_every(c.interval)
          |> Stream.each(fn chunk ->
            File.write!(output_path, chunk, [:append])

            # FIXME: Small race condition here.
            save_chunk(c)
          end)
          |> Stream.run()
        end
      )

      delete(c)
    end
  end

  @doc """
  Simpler lock file which prevents overwriting completed content, and restarts
  from the beginning after interruption.
  """
  @spec run_once(binary(), (-> Enumerable.t()), (Enumerable.t() -> binary()), keyword()) :: :ok
  def run_once(output_path, reader, processor, opts \\ []) do
    c = new(output_path, opts)

    if complete?(c) do
      Logger.debug("Skipping over complete #{output_path}")
      :ok
    else
      if File.exists?(c.checkpoint_path) do
        if File.exists?(output_path) do
          Logger.info("Overwriting incomplete output #{output_path}")
        else
          Logger.debug("Checkpoint but no output. Deleting stale checkpoint.")
        end

        delete(c)
      end

      do_save(c.checkpoint_path, 0)

      report_progress(
        opts[:label] || output_path,
        fn ->
          output =
            reader.()
            |> processor.()

          File.write(output_path, output)
        end
      )

      delete(c)
    end
  end

  defp report_progress(task_name, runner) do
    Logger.info("Processing: #{task_name}")
    runner.()
    Logger.info("Completed: #{task_name}")
  rescue
    err ->
      Logger.error("Failed: #{task_name}")
      reraise err, __STACKTRACE__
  end

  defp complete?(%Opaque{} = c) do
    # If the output exists and there's no checkpoint file, we can assume that the
    # task was already completed.
    #
    # A small kludge also tests for a compressed variant of the output.

    not File.exists?(c.checkpoint_path) and
      (File.exists?(c.output_path) or File.exists?(c.output_path <> ".gz"))
  end

  defp skip(stream, c) do
    resume =
      File.exists?(c.checkpoint_path) &&
        File.read!(c.checkpoint_path) |> String.trim() |> String.to_integer()

    if resume > 0 do
      Logger.info("Resuming from checkpoint at #{resume} lines")
      :counters.put(c.counter, 1, resume)
      Stream.drop(stream, resume)
    else
      stream
    end
  end

  defp save_chunk(c) do
    :counters.add(c.counter, 1, c.interval)
    current_line = :counters.get(c.counter, 1)
    do_save(c.checkpoint_path, current_line)
  end

  defp do_save(checkpoint_path, line_num) do
    File.write!(checkpoint_path, "#{line_num}\n")
  end

  defp checkpoint_path(path), do: "#{Path.dirname(path)}/.lock.#{Path.basename(path)}"

  defp delete(%Opaque{checkpoint_path: path}) do
    File.rm!(path)
  end
end
