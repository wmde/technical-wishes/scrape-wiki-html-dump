defmodule Wiki.Mapdata do
  require Logger

  @spec fetch_and_summarize_mapdata(Wiki.SiteMatrix.Spec.t(), integer()) :: map()
  def fetch_and_summarize_mapdata(site, revid) do
    with {:ok, response} <- measured_fetch_mapdata(site, revid),
         {:ok, mapdata} <- parse_mapdata_response(response) do
      mapdata
      |> analyze_all_mapdata()
    else
      {:error, error} ->
        Logger.warning(
          "Mapdata error on wiki #{site.dbname} for revision #{revid}: #{format_error(error)}"
        )

        %{}
    end
  end

  defp format_error(%Wiki.Error{message: message}), do: message
  defp format_error(error) when is_atom(error), do: Atom.to_string(error)
  defp format_error(error) when is_binary(error), do: error
  defp format_error(_), do: "unknown"

  defp measured_fetch_mapdata(site, revid) do
    :telemetry.span(
      ~w(scraper fetch_mapdata)a,
      %{},
      fn ->
        {
          fetch_mapdata(site, revid),
          %{wiki: site.dbname}
        }
      end
    )
  end

  defp fetch_mapdata(site, revid) do
    site
    |> Wiki.Action.new()
    |> Wiki.Action.get(
      action: :query,
      prop: :mapdata,
      revids: revid
    )
  end

  # This is a normal error occurring when the revision has been deleted since dump time.
  @spec parse_mapdata_response(map()) :: {:ok, map()} | {:error, atom()}
  defp parse_mapdata_response(%{result: %{"query" => %{"badrevids" => _}}}),
    do: {:error, :missing_revision}

  defp parse_mapdata_response(%{result: %{"query" => %{"pages" => []}}}),
    do: {:error, :missing_page}

  defp parse_mapdata_response(%{result: %{"query" => %{"pages" => [%{"mapdata" => nil} | _]}}}),
    do: {:error, :null_mapdata}

  defp parse_mapdata_response(%{result: %{"query" => %{"pages" => [%{"mapdata" => blob} | _]}}}) do
    JiffyEx.decode!(blob, return_maps: true)
    |> case do
      [] -> {:error, :empty_mapdata}
      mapdata -> {:ok, mapdata}
    end
  end

  defp parse_mapdata_response(_), do: {:error, :unknown_missing_mapdata}

  @spec analyze_all_mapdata(map()) :: map()
  def analyze_all_mapdata(mapdata) do
    mapdata
    |> Map.values()
    |> Enum.concat()
    |> Enum.filter(&(&1["type"] == "ExternalData"))
    |> Enum.map(&analyze_mapdata/1)
    |> aggregate_mapdata()
  end

  defp analyze_mapdata(mapdata) do
    %{
      # TODO: Is there a way to get the API to respond with raw mapdata?
      service_and_source:
        mapdata["service"] <>
          cond do
            mapdata["service"] == "page" -> ""
            String.contains?(mapdata["url"], "query=") -> "_query"
            String.contains?(mapdata["url"], "ids=") -> "_ids"
            true -> "_unknown"
          end
    }
  end

  defp aggregate_mapdata(rows) do
    %{
      externaldata:
        rows
        |> Enum.map(& &1.service_and_source)
        |> Enum.frequencies()
    }
  end
end
