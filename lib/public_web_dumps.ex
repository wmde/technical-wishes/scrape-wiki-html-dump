defmodule Wiki.PublicWebDumps do
  @moduledoc """
  Read from the public web server for HTML dumps.
  """
  require Logger
  alias Wiki.DumpsMirror

  @base_url "https://dumps.wikimedia.org/other/enterprise_html/runs/"

  @doc """
  Development-only: fetch dumps and save to local mirror.
  """
  def fetch_missing_dumps(wikis, snapshot) do
    wikis
    |> Enum.each(&fetch_dump(&1, snapshot))
  end

  defp fetch_dump(wiki, snapshot) do
    path = DumpsMirror.find_dump(wiki, snapshot)

    if not File.exists?(path) do
      url = find_dump_for_wiki(wiki, snapshot)
      Logger.info("Fetching dump from #{url} and writing to #{path}")

      File.mkdir_p!(Path.dirname(path))

      Wiki.Network.stream_from_url(url)
      |> Stream.into(File.stream!(path))
      |> Stream.run()
    end
  end

  @doc false
  def find_dump_for_wiki(wiki, snapshot \\ nil) do
    timestamp = snapshot || find_latest_snapshot_date()
    dump_url_for_snapshot(wiki, timestamp)
  end

  def dump_url_for_snapshot(wiki, snapshot) do
    "#{@base_url}#{snapshot}/#{wiki}-NS0-#{snapshot}-ENTERPRISE-HTML.json.tar.gz"
  end

  @doc false
  def find_latest_snapshot_date do
    # FIXME: See https://phabricator.wikimedia.org/T332562, there are issues
    # when we hit an in-progress dump directory.
    HTTPoison.get!(@base_url)
    |> Map.get(:body)
    |> parse_html_top_snapshot()
  end

  def parse_html_top_snapshot(html) do
    html
    |> Floki.parse_document!()
    |> Floki.find("pre > a:last-child")
    |> hd()
    |> Floki.text()
    |> String.trim_trailing("/")
  end
end
