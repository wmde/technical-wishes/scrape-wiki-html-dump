defmodule SplitLines do
  @moduledoc """
  Utility to transform a binary stream with arbitrary chunk boundaries,
  concatenating and splitting into a stream of newline-separated records.
  """

  @spec stream!(Enumerable.t(binary())) :: Enumerable.t(String.t())
  @doc """
  Concatenates incoming binary chunks, splits on newline and emits a stream of
  lines.
  """
  def stream!(input) do
    # Adapted from https://elixirforum.com/t/streaming-lines-from-an-enum-of-chunks/21244
    # Note that this will silently drop the last line if no final newline
    # appears.  An after_fun would be able to handle the remainder, or we could
    # synthesize a newline to the end of input, if necessary.
    input
    |> Stream.transform("", fn chunk, acc ->
      [last_line | lines] =
        (acc <> chunk)
        |> String.split("\n")
        |> Enum.reverse()

      {Enum.reverse(lines), last_line}
    end)
  end
end
