defmodule Wiki.SummaryAggregator do
  @moduledoc """
  Reads rows from the intermediate files with one page per line, creates totals
  and averages and emits as a single JSON blob.
  """
  import Wiki.DumpUtils, only: [ratio: 2]
  alias Wiki.DumpsAnalysis.Plugin.AggregationColumn

  @doc """
  Reduce a stream down to a single sum.
  """
  def aggregate_stream(stream, modules \\ nil) do
    aggregators = get_aggregations(modules)
    initial_values = aggregation_initial_values(aggregators)

    stream
    |> reject_duplicate_revisions()
    |> aggregate_rows(initial_values, aggregators)
    |> apply_finalizers(aggregators)
    |> apply_averages(aggregators)
  end

  defp get_default_modules, do: Application.get_env(:scrape_wiki_dump, :analysis)

  defp get_aggregations(modules) do
    (modules || get_default_modules())
    |> Enum.flat_map(& &1.aggregations())
  end

  @doc """
  Return a list of column headers in the order they should appear in the
  aggregated output.
  """
  @spec aggregate_columns(list(atom()) | nil) :: list(binary())
  def aggregate_columns(modules \\ nil) do
    modules
    |> get_aggregations()
    |> Enum.map(&(&1.column |> Atom.to_string()))
  end

  @spec aggregation_initial_values(list(AggregationColumn.t())) :: map()
  defp aggregation_initial_values(aggregations) do
    aggregations
    |> Enum.filter(&(!is_nil(&1.initial_value)))
    |> Enum.map(&{&1.column, &1.initial_value})
    |> Map.new()
  end

  @spec aggregate_rows(Enumerable.t(), map(), list(AggregationColumn.t())) :: map()
  defp aggregate_rows(rows, totals, aggregators) do
    Enum.reduce(rows, totals, fn row, acc ->
      aggregate_row(row, acc, aggregators)
    end)
  end

  @spec aggregate_row(map(), map(), list(AggregationColumn.t())) :: map()
  defp aggregate_row(row, totals, aggregators) do
    aggregators
    |> Enum.filter(& &1.aggregator)
    |> Enum.map(&{&1.column, &1.aggregator.(row, totals[&1.column])})
    |> Map.new()
  end

  defp apply_finalizers(totals, aggregations) do
    Map.merge(
      totals,
      aggregations
      |> Enum.filter(& &1.finalizer)
      |> Enum.map(&{&1.column, &1.finalizer.(totals)})
      |> Map.new()
    )
  end

  defp apply_averages(totals, aggregations) do
    Map.merge(
      totals,
      aggregations
      |> Enum.filter(& &1.avg)
      |> Enum.map(&{&1.column, ratio(totals[&1.column], totals[&1.avg])})
      |> Map.new()
    )
  end

  defp reject_duplicate_revisions(stream) do
    stream
    |> Stream.transform(
      MapSet.new(),
      fn row, seen_pages ->
        if MapSet.member?(seen_pages, row["pageid"]) do
          {[], seen_pages}
        else
          {[row], MapSet.put(seen_pages, row["pageid"])}
        end
      end
    )
  end
end
