defmodule TarCat do
  @moduledoc """
  Decode an input stream .tar.gz and output the content of all files.  Note that
  this transparently handles archives containing split files.
  """

  @spec stream!(Enumerable.t(binary())) :: Exile.Stream.t()
  @doc """
  Stream from a tar gz file to binary output.
  """
  def stream!(input) do
    # FIXME: When tar is stopped abruptly due to --sample, it complains loudly.
    # This should be suppressed since it's normal operation--but other errors
    # should still be surfaced.
    Exile.stream!(~w(tar xzf - --to-stdout), input: input, ignore_epipe: true)
  end
end
