# scrape wiki html dump

Analyze and summarize Parsoid HTML dumpfiles for Wikimedia projects, with a focus on collecting metadata about how references are used.

Developed by [WMDE Technical Wishes](https://meta.wikimedia.org/wiki/WMDE_Technical_Wishes) as part of our [reusing references](https://meta.wikimedia.org/wiki/WMDE_Technical_Wishes/Reusing_references) project.

## Documentation

Internal code documentation is produced by running `mix docs` and a recent version will be [published](https://wmde.gitlab.io/technical-wishes/scrape-wiki-html-dump/) on gitlab.

## Workflow and output data

If you just want the resulting data without running the jobs yourself, please check for a recent [published dataset](https://analytics.wikimedia.org/published/datasets/one-off/html-dump-scraper-refs/) on the public Wikimedia analytics server.  Read [metrics.md](metrics.md) for detailed descriptions of the output file structure and contents.

Analysis runs in multiple passes: the first pass reads the raw HTML [enterprise dumps](https://dumps.wikimedia.org/other/enterprise_html/) which contain the HTML+RDFa in [Parsoid format](https://www.mediawiki.org/wiki/Specs/HTML), and outputs a "page summary" file for each wiki, which contains results at a high granularity.  The second pass will aggregate over all pages to produce a "wiki summary" for each dump.  Finally, the wikis are aggregated into a single "all wikis" spreadsheet which has the least information and the least granularity, but allows for simple comparison across sites.

When running locally, you'll find the output files under `reports/` .

## Installation

Install OS dependencies if necessary:

```
apt install cmake
```

Install the Elixir language (release 1.15 or newer), common practice is to build the latest from [source](https://elixir-lang.org/install.html#compiling-from-source).  Alternatively, [prebuilt binaries](https://www.erlang-solutions.com/downloads/) may be available for your distribution.

Compile and run project tests:

```
mix deps.get
mix test
```

## Running

The default is to run in development mode (`MIX_ENV=dev`), which will use the latest dump snapshots.  To fetch a dump from the web and run the pipeline, run with the `--fetch` flag:

```
mix run pipeline.exs --fetch hawiki
```

In production (`MIX_ENV=prod`), a concrete snapshot date must be configured.  Run the entire pipeline for all wikis like so:

```
mix run pipeline.exs
```

### Livebook

For an interactive environment similar Jupyter, run the included Livebook.  First, install the livebook server:

```
mix do local.rebar --force, local.hex --force
mix escript.install hex livebook
```

If you get stuck because of running Elixir 1.13, you can install an older version of livebook:

```
mix escript.install hex livebook 0.6.3
```

Make sure your PATH includes escripts.  (If using asdf, run `mix escript` to find the actual directory name and use that here.)

```
export PATH="$PATH:~/.mix/escripts"
```

Then launch the server:

```
livebook server notebooks/demo.livemd
```

### Monitoring

The application will publish its own Prometheus metrics at http://localhost:9568/metrics .  To set up a local Prometheus, use a command like,
```
docker run \
    --network="host" \
    -v ${PWD}/prometheus.sample.yml:/etc/prometheus/prometheus.yml \
    prom/prometheus
```

We will install Prometheus on the runner, accessible by tunneling:
```
ssh -L 9090:localhost:9090 runner.dump-references-processor.eqiad1.wikimedia.cloud
```

Browse to http://localhost:9090 .

### Debugging

To force a crash and write a [dump file](https://www.erlang.org/doc/apps/erts/crash_dump.html), send `kill -USR1` to the root process.

Giving the job a named node makes it possible to connect remotely:
```
elixir --sname findme -S mix run ...
```

Now it can be reached by a local (or tunneled) iex session:
```
iex --sname remote --remsh findme@stat1009
```

Having trouble guessing your full node name?
```
elixir --sname foo -e 'IO.puts(Node.self)'
foo@stat1009
```

Commands in the remote shell context apply to the running pipeline job.  Note that stderr will appear in the original runner's terminal.
