import Config

snapshot = "20240101"
reports_root = "/srv/published/datasets/one-off/html-dump-scraper-refs/#{snapshot}"

config :scrape_wiki_dump,
  dumps_root: "/mnt/data/xmldatadumps/public/other/enterprise_html/runs",
  reports_root: reports_root,
  snapshot: snapshot

config :scrape_wiki_dump, :logger, [
  {:handler, :file_log, :logger_std_h,
   %{
     config: %{
       file: ~c"#{reports_root}/scrape-wiki-dump.log"
     },
     formatter: Logger.Formatter.new(format: "$date $time $metadata[$level] $message\n")
   }}
]
