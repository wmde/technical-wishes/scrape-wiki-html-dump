import Config

config :logger, :default_handler, level: :critical

config :scrape_wiki_dump,
  analysis: [
    Wiki.DumpPipelineTest.MockAnalysis
  ]

config :scrape_wiki_dump,
  dumps_root: "#{System.tmp_dir!()}/dumps",
  snapshot: "20230102"

config :scrape_wiki_dump,
  reports_root: "#{System.tmp_dir!()}/reports"

config :http_stream, adapter: Wiki.NetworkTest.MockHttpStreamAdapter
