import Config

config :scrape_wiki_dump,
  analysis: [
    Wiki.DumpsAnalysis.General,
    Wiki.DumpsAnalysis.SiteStats,
    # Wiki.DumpsAnalysis.KartographerMaps,
    # Wiki.DumpsAnalysis.ProseSize,
    Wiki.DumpsAnalysis.CiteRefs
  ],
  prometheus_metrics_port: 9568,
  reports_root: "./reports",
  user_agent: "scrape-wiki-dump <techwish@wikimedia.de>"

# TODO: Library should select the adapter dynamically according to whatever is
# available.
config :http_stream, adapter: HTTPStream.Adapter.HTTPoison

config :logger, :default_handler, level: :info

config :logger, :default_formatter, format: "$time $metadata[$level] $message\n"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
