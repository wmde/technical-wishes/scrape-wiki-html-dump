import Config

config :scrape_wiki_dump,
  dumps_root: "./dumps",
  snapshot: :latest

config :scrape_wiki_dump, :logger, [
  {:handler, :file_log, :logger_std_h,
   %{
     config: %{
       file: ~c"./run.log"
     },
     formatter: Logger.Formatter.new(format: "$date $time $metadata[$level] $message\n")
   }}
]
