defmodule ScrapeWikiDump.MixProject do
  use Mix.Project

  def project do
    [
      app: :scrape_wiki_dump,
      version: "0.3.1",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      dialyzer: [
        plt_add_apps: ~w(mediawiki_client)a,
        plt_core_path: "priv/plts/"
      ],
      docs: [
        extras: ~w(CHANGELOG.md notebooks/demo.livemd README.md metrics.md),
        main: "readme"
      ],
      source_url: "https://gitlab.com/wmde/technical-wishes/scrape-wiki-html-dump",
      test_coverage: [tool: ExCoveralls],
      deps: deps(),
      aliases: aliases(),
      preferred_cli_env: [
        "coveralls.html": :test,
        "test.all": :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {ScrapeWikiDump.Application, []},
      extra_applications: [:logger]
    ]
  end

  defp aliases do
    [
      "test.all": [
        "format --check-formatted",
        "clean",
        "compile --warnings-as-errors",
        "credo --strict",
        "coveralls.html",
        "dialyzer",
        "doctor"
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.7", only: :test, runtime: false},
      {:csv, "~> 3.0"},
      {:dialyxir, "~> 1.4", only: :test, runtime: false},
      {:doctor, "~> 0.21", only: :test, runtime: false},
      {:ex_doc, "~> 0.30", only: :dev, runtime: false},
      {:excoveralls, "~> 0.17", only: :test, runtime: false},
      {:exile, "~> 0.7"},
      # TODO: Compare performance of html5ever backend, especially memory usage
      {:fast_html, "~> 2.2"},
      {:floki, "~> 0.34"},
      {:flow, "~> 1.2"},
      # FIXME: Can't use newer httpoison because of http_stream
      {:httpoison, "~> 1.7"},
      {:http_stream, "~> 1.0"},
      {:jason, "~> 1.4"},
      {:jiffy_ex, "~> 1.1"},
      {:levenshtein, "~> 0.3"},
      {:mediawiki_client, "~> 0.4", runtime: false},
      {:mox, "~> 1.1", only: :test},
      {:owl, "~> 0.7"},
      {:stream_gzip, "~> 0.4"},
      {:telemetry, "~> 1.0"},
      {:telemetry_metrics, "~> 1.0"},
      {:telemetry_metrics_prometheus, "~> 1.1.0"},
      {:telemetry_poller, "~> 1.0"}
    ]
  end
end
