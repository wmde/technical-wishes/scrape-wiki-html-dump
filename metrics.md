This documents all columns appearing in output files.

## Page summary columns

Fine-grained -"page-summary" files are encoded as NDJSON, where each line represents a summarized page and each file corresponds to a wiki.

These page-summary files have the following fields:

**automatic_ref_name_usages_count** - Number of ref tags which have a ":0", ":1", etc. automatic reference name.  (Includes reuses.)

**automatic_ref_names_count** - Number of unique ref names matching ":0", ":1", etc. (these are created automatically by VE).

**identical_ref_count** - Number of refs which have exactly the same content as another ref, but are not a reuse.  For a set of four identical refs, this count is 3.

**list_defined_ref_count** - Number of ref tags where the content is defined inside of a `<references>` block.

**nested_ref_count** - Number of ref tags that are part of another ref's body content.

**mapframe_count** - Number of rendered mapframe tags.

**mapframe_from_transclusion_count** - Number of mapframe tags which are produced by a transclusion.

**maplink_count** - Number of rendered maplink tags.

**maplink_from_transclusion_count** - Number of maplink tags which are produced by a transclusion.

**potential_ref_transclusions** - List of transclusions which seem to produce refs, as a map including the frequencies of each template.

**potential_transclusions_with_top_level_refs** - List of transclusions which seem to produce refs directly on the top level of the output, as a map including the frequencies of each template. Subset of the former.

**proportion_of_named_refs_uniquely_named** - How many named refs have a unique name (50% when each named ref is only reused once).  When averaged across the wiki, the per-page proportions are averaged rather than recalculating using the total number of named and reused refs on the wiki.

**proportion_of_refs_named** - Proportion of named refs compared to the total number of refs on each page.  Per-page proportions are averaged across the wiki.

**prose_size** - Length of the page content after non-main text blocks and all markup are stripped out.  Given as a Unicode-aware string size.

**ref_by_transclusion_count** - Number of ref tags which were produced by templates.

**ref_by_top_ref_transclusion_count** - Number of ref tags which were produced by templates directly at the top level of the template output. Subset of the former.

**ref_count** - Number of footnote markers appearing in the article body.

**ref_error_counts_by_type** - List of each ref error and how many times it occurs on the page.

**reflist_count** - Number of rendered `<references>` tags, including the implicit default reflist.

**ref_reuse_counts** - List with the number of reuses for each unique ref name having at least one reuse.  Note that the number of reuses is (number of matching refs - 1), and that refs with zero reuses are excluded.

**refs_with_solely_transclusion_count** - Number of ref tags which include exactly one template and nothing else inside the footnote body.

**refs_with_transclusions_count** - Number of ref tags which include a template inside the footnote body.

**ref_with_name_count** - Number of footnote markers which have a `name` attribute.

**revid** - Revision ID key for per-page files.

**similar_ref_count** - Number of refs with similar but not identical text as another ref, where similarity is measured as: differing by less than 10 characters in length, and with a Levenshtein edit distance less than 10% of the longer text.

**title** - Page title.

**transclusion_count** - Number of templates used on the page, including nested templates.

**transclusions_inside_refs** - List of templates which are used inside of a ref tag, and their frequency.

**unique_name_count** - Number of unique ref names on the page.

**wikitext_length** - Page wikitext length in bytes.

**word_count** - Number of whitespace-separated words in page "prose".  Note that the number has no meaning for languages such as Chinese which don't use whitespace between words.

## Wiki summary

Each page-summary file is then rolled up into an aggregate of per-wiki totals.  Many of the fields above are rolled up, as a -"sum" when the values are added together, and as an -"average" when divided by the number of pages.

**article_count_stats** - Article count reported by the site's own API.

**identical_refs_count** - Total number of refs which were identical to another on the page.

**identical_refs_on_pages_with_25_or_less_refs_average** - Average number of identical refs on pages with 25 or less ref tags.

**identical_refs_on_pages_with_over_25_refs_average** - Average number of identical refs on pages with more than 25 ref tags.

**identical_refs_on_pages_with_over_25_refs_count** - Total number of identical refs across all pages with more than 25 refs.

**list_defined_ref_per_page_having_ref** - Average number of list-defined references, per page with references.

**list_defined_ref_sum** - Total number of list-defined references.

**mapframe_from_transclusion_sum** - Total number of mapframes generated by transclusions.

**mapframe_sum** - Total number of mapframes on the wiki.

**maplink_from_transclusion_sum** - Total number of maplinks generated by transclusions.

**maplink_sum** - Total number of maplinks on the wiki.

**max_ref_reuse_average** - Average number of reuses for the most-reused ref on each page where refs are reused.

**nested_ref_sum** - Total number of ref tags that are part of another ref's body content.

**page_count** - Total number of pages scraped.

**pages_with_automatically_named_refs_count** - Pages on which at least one ref tag has an automatic name like ":0".

**pages_with_identical_refs_count** - Number of pages where at least one pair of refs have the same body content.

**pages_with_identical_refs_and_over_25_refs_count** - Number of pages with at least one pair of identical refs, and more than 25 refs total.

**pages_with_maps_count** - Number of pages on which a mapframe or maplink appears.

**pages_with_multiple_reflists_count** - Number of pages with more than one reflist section.

**pages_with_named_refs_count** - Number of pages where at least one ref is named.

**pages_with_nested_refs_count** - Number of pages where at least one ref has another ref as part of its body content.

**pages_with_over_25_refs_count** - Number of pages with more than 25 ref tags.

**pages_with_ref_reuse_count** - Number of pages on which at least one ref is reused.

**pages_with_refs_count** - Number of pages with at least one ref.

**pages_with_similar_refs_count** - Number of pages where at least one pair of refs had similar but not identical content.

**potential_ref_transclusions** - Frequency of transclusions that are seen producing ref tags, as a map from template name to count.

**potential_transclusions_with_top_level_refs** - List of transclusions which seem to produce refs directly on the top level of the output, as a map including the frequencies of each template. Subset of the former.

**proportion_of_named_refs_uniquely_named_average** - Average of `proportion_of_named_refs_uniquely_named` across pages with named refs.

**proportion_of_pages_with_identical_refs** -Proportion of all pages where at least one pair of refs have the same body content.

**proportion_of_pages_with_nested_refs** - Proportion of all pages which contain at least one ref that has another ref as part of its body content.

**proportion_of_pages_with_similar_refs** - Proportion of all pages where at least one pair of refs had similar but not identical content.

**proportion_of_pages_with_refs** - Proportion of all pages which contain at least one ref.

**proportion_of_refs_from_transclusion** - Proportion of all refs which are produced by a transclusion.

**proportion_of_refs_having_transclusion** - Proportion of all refs which contain a transclusion.

**proportion_of_refs_named_average** - Average of the proportion of refs which are named on each page, across all pages with refs.

**proportion_of_refs_reused_average** - Average of the proportion of refs which are reused on each page, across all pages with refs.

**prose_size_average** - Average prose size across pages.

**ref_by_transclusion_average** - Average number of refs produced by a transclusion, per page having refs.

**ref_by_transclusion_count** - Total number of ref tags produced by templates.

**ref_count** - Total number of footnote markers appearing on the wiki.

**ref_count_per_page** - Average number of refs per page.

**ref_count_per_page_having_ref** - Average number of refs on pages with at least one ref.

**ref_error_average** - Average number of ref errors over pages with refs.

**ref_error_counts_by_type** - List of each ref error and how many times it occurs on all pages.

**reflist_count** - Total number of `<references>` tags on all pages.

**reflists_per_page_having_ref** - Average number of reflists, for pages having refs.

**refs_with_solely_transclusion_count** - Number of ref tags which include exactly one template and nothing else inside the footnote body.

**refs_with_transclusions_count** - Number of ref tags which include a template inside the footnote body.

**similar_refs_count** - Total number of refs which were similar to another on the page.

**transclusion_average** - Average number of transclusions per page.

**transclusions_inside_refs** - Frequency list of transclusions found inside of refs.

**transclusion_sum** - Total number of transclusions on the wiki.

**wikitext_length_average** - Average wikitext length across pages.

**word_count_average** - Average word count across pages.

## All-wiki comparisons

Finally, the wiki summaries are pivoted into a CSV file in which each row is a wiki, and each column is an aggregate metric.  Only scalar columns are included at the moment.

Cite errors are summed per error code and per wiki.
