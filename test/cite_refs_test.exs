defmodule Wiki.DumpsAnalysis.CiteRefsTest do
  use ExUnit.Case, async: true
  alias Wiki.DumpUtils
  alias Wiki.SummaryAggregator

  import Wiki.TestUtils

  test "read html page with systematic Cite cases" do
    # This fixture has a mixture of bare refs which include templates, reuses,
    # errors, and one ref created by a template.
    # Generated from the docker-dev sample using REST which is a slightly
    # different formatting of the same data as in dumps.
    #
    # http://dev.wiki.local.wmftest.net:8080/w/rest.php/v1/page/Cite/html
    %{
      automatic_ref_names_count: 0,
      automatic_ref_name_usages_count: 0,
      identical_ref_count: 1,
      list_defined_ref_count: 2,
      nested_ref_count: 0,
      potential_ref_transclusions: %{"RefA" => 2, "RefB" => 2},
      potential_transclusions_with_top_level_refs: %{"RefA" => 1, "RefB" => 1},
      ref_by_transclusion_count: 4,
      ref_by_top_ref_transclusion_count: 2,
      ref_count: 16,
      ref_error_counts_by_type: %{
        "cite_error_references_duplicate_key" => 1,
        "cite_error_references_no_text" => 1
      },
      reflist_count: 3,
      ref_reuse_counts: [1, 1, 1],
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 0,
      ref_with_name_count: 10,
      similar_ref_count: 1,
      transclusion_count: 3,
      transclusions_inside_refs: %{},
      unique_name_count: 7
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/fixture4.html")
    )
  end

  test "read html page with refs containing templates" do
    # Actual page which has a mix of templates contained within refs, for
    # example {{Cite web}} as the entire footnote body, refs including a
    # template as well as wikitext, and refs without templates.
    #
    # https://en.wikipedia.org/api/rest_v1/page/html/Gabriele_Berg/1136276019
    %{
      automatic_ref_names_count: 0,
      automatic_ref_name_usages_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{},
      potential_transclusions_with_top_level_refs: %{},
      ref_by_transclusion_count: 0,
      ref_by_top_ref_transclusion_count: 0,
      ref_count: 6,
      ref_error_counts_by_type: %{},
      reflist_count: 1,
      ref_reuse_counts: [],
      refs_with_solely_transclusion_count: 5,
      refs_with_transclusions_count: 5,
      ref_with_name_count: 0,
      similar_ref_count: 0,
      transclusion_count: 20,
      transclusions_inside_refs: %{"Cite web" => 4, "Google scholar id" => 1},
      unique_name_count: 0
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/fixture6.html")
    )
  end

  test "read page with automatically-named refs" do
    # Adapted from https://en.wikipedia.org/api/rest_v1/page/html/Petronius/1145533698
    %{
      automatic_ref_names_count: 1,
      automatic_ref_name_usages_count: 2,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{},
      potential_transclusions_with_top_level_refs: %{},
      ref_by_transclusion_count: 0,
      ref_by_top_ref_transclusion_count: 0,
      ref_count: 2,
      ref_error_counts_by_type: %{},
      reflist_count: 0,
      ref_reuse_counts: [1],
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 0,
      ref_with_name_count: 2,
      similar_ref_count: 0,
      transclusion_count: 4,
      transclusions_inside_refs: %{},
      unique_name_count: 1
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/fixture7.html")
    )
  end

  test "parse dump line with cite errors and transcluded refs" do
    # This fixture has a mixture of bare refs which include templates, reuses,
    # errors, and one ref created by a template.
    # See https://ha.wikipedia.org/w/index.php?oldid=193821
    %{
      automatic_ref_names_count: 0,
      automatic_ref_name_usages_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{
        "Quote box" => 1,
        "Col-2" => 2,
        "Col-begin" => 1,
        "Col-end" => 1
      },
      potential_transclusions_with_top_level_refs: %{},
      ref_by_transclusion_count: 5,
      ref_by_top_ref_transclusion_count: 0,
      ref_count: 15,
      ref_error_counts_by_type: %{"cite_error_references_no_text" => 10},
      reflist_count: 1,
      ref_reuse_counts: [3, 2, 1],
      refs_with_solely_transclusion_count: 3,
      refs_with_transclusions_count: 3,
      ref_with_name_count: 13,
      similar_ref_count: 0,
      transclusion_count: 11,
      transclusions_inside_refs: %{"Cite journal" => 1, "Cite web" => 2},
      unique_name_count: 7
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_dump_file("./test/data/fixture1.ndjson")
    )
  end

  test "parse dump line with list-defined references" do
    # This fixture has multiple reflists and includes a LDR
    # See https://ha.wikipedia.org/w/index.php?oldid=199390
    %{
      automatic_ref_names_count: 0,
      automatic_ref_name_usages_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 1,
      nested_ref_count: 0,
      potential_ref_transclusions: %{},
      potential_transclusions_with_top_level_refs: %{},
      ref_by_transclusion_count: 0,
      ref_by_top_ref_transclusion_count: 0,
      ref_count: 18,
      ref_error_counts_by_type: %{},
      reflist_count: 2,
      ref_reuse_counts: [],
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 1,
      ref_with_name_count: 1,
      similar_ref_count: 0,
      transclusion_count: 3,
      transclusions_inside_refs: %{"Webarchive" => 1},
      unique_name_count: 1
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_dump_file("./test/data/fixture2.ndjson")
    )
  end

  test "parse transclusion with template node not in first position (T366436)" do
    %{
      automatic_ref_name_usages_count: 0,
      automatic_ref_names_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{"Sfn" => 1},
      potential_transclusions_with_top_level_refs: %{"Sfn" => 1},
      ref_by_top_ref_transclusion_count: 1,
      ref_by_transclusion_count: 1,
      ref_count: 1,
      ref_error_counts_by_type: %{},
      ref_reuse_counts: [],
      ref_with_name_count: 1,
      reflist_count: 0,
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 0,
      similar_ref_count: 0,
      transclusion_count: 2,
      transclusions_inside_refs: %{},
      unique_name_count: 1
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/template-not-first.html")
    )
  end

  test "similar refs are detected and counted once each" do
    # This fixture has a mixture of bare refs which include templates, reuses,
    # errors, and one ref created by a template.
    # Generated from the docker-dev sample using REST which is a slightly
    # different formatting of the same data as in dumps.
    #
    # http://dev.wiki.local.wmftest.net:8080/w/rest.php/v1/page/Cite/html
    %{
      automatic_ref_names_count: 0,
      automatic_ref_name_usages_count: 0,
      identical_ref_count: 3,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{},
      potential_transclusions_with_top_level_refs: %{},
      ref_by_transclusion_count: 0,
      ref_by_top_ref_transclusion_count: 0,
      ref_count: 8,
      ref_error_counts_by_type: %{},
      reflist_count: 1,
      ref_reuse_counts: [],
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 0,
      ref_with_name_count: 0,
      similar_ref_count: 3,
      transclusion_count: 0,
      transclusions_inside_refs: %{},
      unique_name_count: 0
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/fixture8.html")
    )
  end

  test "refs transcluded from the top level of a template" do
    # https://en.wikipedia.beta.wmflabs.org/api/rest_v1/page/html/User:Fisch-WMDE%2Fsandbox%2FRefFromTemplate/617685
    %{
      automatic_ref_names_count: 0,
      automatic_ref_name_usages_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{"RefA" => 1},
      potential_transclusions_with_top_level_refs: %{"RefA" => 1},
      ref_by_transclusion_count: 1,
      ref_by_top_ref_transclusion_count: 1,
      ref_count: 2,
      ref_error_counts_by_type: %{},
      reflist_count: 1,
      ref_reuse_counts: [1],
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 0,
      ref_with_name_count: 2,
      similar_ref_count: 0,
      transclusion_count: 2,
      transclusions_inside_refs: %{},
      unique_name_count: 1
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/top-level-reference-transclusion.html")
    )
  end

  test "templates as only content of a references" do
    # https://en.wikipedia.beta.wmflabs.org/api/rest_v1/page/html/User:Fisch-WMDE%2Fsandbox%2FTemplateInRef/617768
    %{
      automatic_ref_names_count: 0,
      automatic_ref_name_usages_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{"RefB" => 1},
      potential_transclusions_with_top_level_refs: %{},
      ref_by_transclusion_count: 1,
      ref_by_top_ref_transclusion_count: 0,
      ref_count: 3,
      ref_error_counts_by_type: %{},
      reflist_count: 1,
      ref_reuse_counts: [],
      refs_with_solely_transclusion_count: 1,
      refs_with_transclusions_count: 2,
      ref_with_name_count: 1,
      similar_ref_count: 0,
      transclusion_count: 3,
      transclusions_inside_refs: %{"Existing Template" => 2},
      unique_name_count: 1
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/templates-in-refs.html")
    )
  end

  test "references nested in references" do
    # https://en.wikipedia.beta.wmflabs.org/api/rest_v1/page/html/User:Fisch-WMDE%2Fsandbox%2FRefFromTemplateNesting/618421
    %{
      automatic_ref_names_count: 0,
      automatic_ref_name_usages_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 7,
      potential_ref_transclusions: %{"#tag:ref" => 4, "RefA" => 3},
      potential_transclusions_with_top_level_refs: %{"#tag:ref" => 4, "RefA" => 3},
      ref_by_transclusion_count: 7,
      ref_by_top_ref_transclusion_count: 7,
      ref_count: 14,
      ref_error_counts_by_type: %{},
      reflist_count: 2,
      ref_reuse_counts: [],
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 0,
      ref_with_name_count: 14,
      similar_ref_count: 4,
      transclusion_count: 7,
      transclusions_inside_refs: %{},
      unique_name_count: 14
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/reference-nesting.html")
    )
  end

  test "references with top-level multinode template" do
    %{
      automatic_ref_name_usages_count: 0,
      automatic_ref_names_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{},
      potential_transclusions_with_top_level_refs: %{},
      ref_by_top_ref_transclusion_count: 0,
      ref_by_transclusion_count: 0,
      ref_count: 1,
      ref_error_counts_by_type: %{},
      ref_reuse_counts: [],
      ref_with_name_count: 0,
      reflist_count: 1,
      refs_with_solely_transclusion_count: 1,
      refs_with_transclusions_count: 1,
      similar_ref_count: 0,
      transclusion_count: 1,
      transclusions_inside_refs: %{"Multinode" => 1},
      unique_name_count: 0
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/template-multinode.html")
    )
  end

  test "template producing a ref tag at the top level but not first" do
    %{
      automatic_ref_name_usages_count: 0,
      automatic_ref_names_count: 0,
      identical_ref_count: 0,
      list_defined_ref_count: 0,
      nested_ref_count: 0,
      potential_ref_transclusions: %{"RefLater" => 1},
      potential_transclusions_with_top_level_refs: %{},
      ref_by_top_ref_transclusion_count: 0,
      ref_by_transclusion_count: 1,
      ref_count: 1,
      ref_error_counts_by_type: %{},
      ref_reuse_counts: [],
      ref_with_name_count: 0,
      reflist_count: 1,
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 0,
      similar_ref_count: 0,
      transclusion_count: 1,
      transclusions_inside_refs: %{},
      unique_name_count: 0
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.CiteRefs,
      parse_html_file("./test/data/template-refsecond.html")
    )
  end

  test "normalizes HTML comments in template name" do
    actual =
      "foo <!-- \n bar -->\n"
      |> Wiki.DumpsAnalysis.CiteRefs.normalize_template_name()

    assert "Foo" == actual
  end

  test "normalizes underscore and whitespace in template name" do
    actual =
      "Cite__web"
      |> Wiki.DumpsAnalysis.CiteRefs.normalize_template_name()

    assert "Cite web" == actual
  end

  test "aggregate" do
    expected = %{
      identical_refs_count: 1,
      identical_refs_on_pages_with_25_or_less_refs_average: 0.3333,
      identical_refs_on_pages_with_over_25_refs_average: 0,
      identical_refs_on_pages_with_over_25_refs_count: 0,
      list_defined_ref_per_page_having_ref: 0.0,
      list_defined_ref_sum: 0,
      max_ref_reuse_average: 3.0,
      nested_ref_sum: 7,
      page_count: 4,
      pages_with_automatically_named_refs_count: 4,
      pages_with_identical_refs_and_over_25_refs_count: 0,
      pages_with_identical_refs_count: 1,
      pages_with_multiple_reflists_count: 1,
      pages_with_named_refs_count: 2,
      pages_with_nested_refs_count: 1,
      pages_with_over_25_refs_count: 0,
      pages_with_ref_reuse_count: 1,
      pages_with_refs_count: 3,
      pages_with_similar_refs_count: 2,
      potential_ref_transclusions: [["updated", 4], ["efn", 2]],
      potential_transclusions_with_top_level_refs: [["RefA", 2], ["RefB", 2]],
      proportion_of_named_refs_uniquely_named_average: 0.75,
      proportion_of_pages_with_identical_refs: 0.3333,
      proportion_of_pages_with_nested_refs: 0.3333,
      proportion_of_pages_with_refs: 0.75,
      proportion_of_pages_with_similar_refs: 0.6667,
      proportion_of_refs_from_transclusion: 0.3043,
      proportion_of_refs_having_transclusion: 0.2174,
      proportion_of_refs_named_average: 0.0883,
      proportion_of_refs_reused_average: 0.0256,
      ref_by_transclusion_average: 2.3333,
      ref_by_transclusion_count: 7,
      ref_count: 23,
      ref_count_per_page: 5.75,
      ref_count_per_page_having_ref: 7.6667,
      ref_error_average: 1.0,
      ref_error_counts_by_type: %{
        "cite_error_group_refs_without_references" => 2,
        "cite_error_references_no_text" => 1
      },
      reflist_count: 4,
      reflists_per_page_having_ref: 1.3333,
      refs_with_solely_transclusion_count: 0,
      refs_with_transclusions_count: 5,
      similar_refs_count: 3,
      transclusion_sum: 29,
      transclusion_average: 7.25,
      transclusions_inside_refs: [
        ["cite news", 1],
        ["cite web", 1],
        ["dead link", 1],
        ["nft", 1],
        ["nft player", 1],
        ["soccerway", 1]
      ],
      wiki: "testwiki",
      wikitext_length_average: 202
    }

    actual =
      "./test/data/fixture5-summary.ndjson"
      |> DumpUtils.stream_binary_file()
      |> SplitLines.stream!()
      |> DumpUtils.decode_ndjson()
      |> SummaryAggregator.aggregate_stream([
        Wiki.DumpsAnalysis.General,
        Wiki.DumpsAnalysis.CiteRefs
      ])

    assert expected == actual
  end
end
