defmodule Wiki.DumpsAnalysis.SiteStatsTest do
  use ExUnit.Case, async: true
  import ExUnit.CaptureLog
  import Mox
  import Wiki.TestUtils

  test "fetching article count from api response" do
    #  https://www.mediawiki.org/w/api.php?action=query&meta=siteinfo&siprop=statistics
    Wiki.Action.MockAdapter
    |> expect(:call, fn env, _opts ->
      {:ok,
       %Tesla.Env{
         env
         | body: %{
             "batchcomplete" => "",
             "query" => %{
               "statistics" => %{
                 "pages" => 1_632_135,
                 "articles" => 63_005,
                 "edits" => 6_304_227,
                 "images" => 2_797,
                 "users" => 17_905_858,
                 "activeusers" => 927,
                 "admins" => 133,
                 "jobs" => 0,
                 "cirrussearch-article-words" => 27_843_642,
                 "queued-massmessages" => 0
               }
             }
           },
           headers: [],
           status: 200
       }}
    end)

    assert 63_005 == Wiki.DumpsAnalysis.SiteStats.get_number_of_articles(test_site())
  end

  test "server error while fetching article count" do
    Wiki.Action.MockAdapter
    |> expect(:call, fn env, _opts ->
      {:ok, %Tesla.Env{env | body: %{}, headers: [], status: 500}}
    end)

    logs =
      capture_log(fn ->
        assert -1 == Wiki.DumpsAnalysis.SiteStats.get_number_of_articles(test_site())
      end)

    assert logs =~ "Error received with HTTP status 500"
  end
end
