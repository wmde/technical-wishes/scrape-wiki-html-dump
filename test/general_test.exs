defmodule GeneralTest do
  use ExUnit.Case

  import Wiki.TestUtils

  test "parse dump line" do
    expected = %{
      html_length: 74_010,
      pageid: 34_170,
      revid: 193_821,
      title: "Fantasy Black Channel",
      wiki: "test",
      wikitext_length: 24_626
    }

    raw_line =
      File.read!("./test/data/fixture1.ndjson")
      |> JiffyEx.decode!(return_maps: true)

    result =
      %{raw_line: raw_line, site: test_site()}
      |> Wiki.DumpsAnalysis.General.analyze_page()
      |> Map.drop([:dom])

    assert expected == result
  end
end
