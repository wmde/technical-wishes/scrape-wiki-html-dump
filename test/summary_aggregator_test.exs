defmodule Wiki.SummaryAggregatorTest do
  use ExUnit.Case
  alias Wiki.DumpUtils
  alias Wiki.SummaryAggregator

  test "read and aggregate intermediate file" do
    expected = %{
      page_count: 4,
      wiki: "testwiki",
      wikitext_length_average: 202.0
    }

    actual =
      "./test/data/fixture5-summary.ndjson"
      |> DumpUtils.stream_binary_file()
      |> SplitLines.stream!()
      |> DumpUtils.decode_ndjson()
      |> SummaryAggregator.aggregate_stream([
        Wiki.DumpsAnalysis.General
      ])

    assert expected == actual
  end
end
