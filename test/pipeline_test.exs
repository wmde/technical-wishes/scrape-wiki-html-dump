defmodule Wiki.DumpPipelineTest do
  use ExUnit.Case
  import Mox

  setup :set_mox_from_context
  setup :verify_on_exit!

  defp reset_reports() do
    reports_root = Application.fetch_env!(:scrape_wiki_dump, :reports_root)
    assert(String.starts_with?(reports_root, "/tmp"))
    File.rm_rf!(reports_root)
    File.mkdir_p!(reports_root)
    reports_root
  end

  test "run pipeline integration" do
    dbname = "testwiki"
    snapshot = "20230101"

    dump_path = Wiki.DumpsMirror.find_dump(dbname, snapshot)
    File.mkdir_p!(Path.dirname(dump_path))
    File.copy!("./test/data/fixture9.tar.gz", dump_path)

    reports_root = reset_reports()

    Wiki.DumpPipelineTest.MockAnalysis
    |> expect(:analyze_page, fn %{raw_line: %{"a" => 1, "b" => 2}} ->
      %{c: 5, revid: 1, pageid: 1}
    end)
    |> expect(:analyze_page, fn %{raw_line: %{"a" => 3, "b" => 4}} ->
      %{c: 6, revid: 2, pageid: 2}
    end)
    |> expect(:aggregations, 2, fn ->
      [
        Wiki.DumpsAnalysis.Plugin.aggregate(:c, 0, fn row, acc -> row["c"] + acc end)
      ]
    end)

    Wiki.Action.MockAdapter
    |> expect(:call, fn env, _opts ->
      {:ok,
       %Tesla.Env{
         env
         | body: %{
             "sitematrix" => %{
               "count" => 1,
               "0" => %{
                 "code" => "aa",
                 "name" => "Qafár af",
                 "site" => [
                   %{
                     "url" => "https://wiki.test",
                     "dbname" => "testwiki",
                     "lang" => "test",
                     "sitename" => "Wikipedia",
                     "closed" => true
                   }
                 ],
                 "dir" => "ltr",
                 "localname" => "Afar"
               }
             }
           },
           headers: [],
           status: 200
       }}
    end)

    # FIXME: Makes an external request to the real sitematrix
    Wiki.DumpPipeline.run_pipeline(
      [dbname],
      snapshot,
      adapter: Wiki.Action.MockAdapter
    )

    result =
      File.stream!("#{reports_root}/all-wikis-#{snapshot}-summary.csv")
      |> CSV.decode!(headers: true)
      |> Enum.to_list()

    assert [%{"c" => "11"}] == result
  end

  test "aggregate errors" do
    snapshot = "20230101"
    reports_root = reset_reports()

    [
      %{
        "wiki" => "aawiki",
        "ref_error_counts_by_type" => %{
          "cite_error_references_duplicate_key" => 2
        },
        "unused" => 123
      },
      %{
        "wiki" => "bbwiki",
        "ref_error_counts_by_type" => %{
          "cite_error_references_no_text" => 873,
          "cite_error_references_duplicate_key" => 29
        }
      }
    ]
    |> Wiki.DumpPipeline.aggregate_errors(snapshot)

    result =
      File.stream!("#{reports_root}/all-wikis-#{snapshot}-cite-error-summary.csv")
      |> CSV.decode!(headers: true)
      |> Enum.to_list()

    expected =
      [
        %{
          "cite_error_references_duplicate_key" => "2",
          "cite_error_references_no_text" => "0",
          "wiki" => "aawiki"
        },
        %{
          "cite_error_references_duplicate_key" => "29",
          "cite_error_references_no_text" => "873",
          "wiki" => "bbwiki"
        }
      ]

    assert expected == result
  end
end
