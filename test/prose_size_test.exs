defmodule Wiki.DumpsAnalysis.ProseSizeTest do
  use ExUnit.Case, async: true
  alias Wiki.DumpUtils
  alias Wiki.SummaryAggregator

  import Wiki.TestUtils

  test "analyze prose size" do
    # Adapted from https://en.wikipedia.org/api/rest_v1/page/html/Petronius/1145533698
    %{
      prose_size: 531,
      word_count: 99
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.ProseSize,
      parse_html_file("./test/data/fixture7.html")
    )
  end

  test "aggregate" do
    expected = %{
      page_count: 4,
      prose_size_average: 150.75,
      wiki: "testwiki",
      wikitext_length_average: 202.0,
      word_count_average: 10.75
    }

    actual =
      "./test/data/fixture5-summary.ndjson"
      |> DumpUtils.stream_binary_file()
      |> SplitLines.stream!()
      |> DumpUtils.decode_ndjson()
      |> SummaryAggregator.aggregate_stream([
        Wiki.DumpsAnalysis.General,
        Wiki.DumpsAnalysis.ProseSize
      ])

    assert expected == actual
  end
end
