defmodule SplitLinesTest do
  use ExUnit.Case, async: true

  test "repack into lines" do
    result =
      ["a\nbc", "d\nefg", "hi\nj", "\nk\n"]
      |> SplitLines.stream!()
      |> Enum.to_list()

    assert ~w(a bcd efghi j k) == result
  end
end
