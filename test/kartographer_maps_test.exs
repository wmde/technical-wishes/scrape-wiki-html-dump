defmodule Wiki.DumpsAnalysis.KartographerMapsTest do
  use ExUnit.Case, async: true
  alias Wiki.DumpUtils
  alias Wiki.SummaryAggregator

  import Wiki.TestUtils
  import Mox

  setup :verify_on_exit!

  test "read page without maps" do
    %{
      mapframe_count: 0,
      mapframe_from_transclusion_count: 0,
      maplink_count: 0,
      maplink_from_transclusion_count: 0
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.KartographerMaps,
      parse_html_file("./test/data/fixture4.html")
    )
  end

  test "read page with transcluded inline maps" do
    Wiki.Action.MockAdapter
    |> expect(:call, fn env, _opts ->
      {:ok, %Tesla.Env{env | body: %{}, headers: [], status: 500}}
    end)

    # https://en.wikipedia.beta.wmflabs.org/api/rest_v1/page/html/Maptests/572894
    %{
      mapframe_count: 14,
      mapframe_from_transclusion_count: 8,
      maplink_count: 3,
      maplink_from_transclusion_count: 0
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.KartographerMaps,
      parse_html_file("./test/data/Maptests.html")
    )
  end

  test "read page with inline and external maps" do
    Wiki.Action.MockAdapter
    |> expect(:call, fn env, _opts ->
      assert env.query == [
               action: :query,
               format: :json,
               formatversion: 2,
               prop: :mapdata,
               revids: 123
             ]

      # https://www.mediawiki.org/w/api.php?action=query&prop=mapdata&formatversion=2&revids=5843699&mpdgroups=_909c5422c015cc9e3fbd3ba921f7a42fae089606|_7df70a5311c282cf1c1088b0e788335f01f1bdcd
      {:ok,
       %Tesla.Env{
         env
         | body: %{
             "batchcomplete" => true,
             "query" => %{
               "pages" => [
                 %{
                   "pageid" => 564_647,
                   "ns" => 12,
                   "title" => "Help:Extension:Kartographer",
                   "revid" => 5_843_699,
                   "mapdata" => [
                     Jason.encode!(%{
                       "_7df70a5311c282cf1c1088b0e788335f01f1bdcd" => [
                         %{
                           type: "ExternalData",
                           service: "geopoint",
                           url:
                             "https://maps.wikimedia.org/geopoint?getgeojson=1&ids=Q667450%2CQ15958518"
                         }
                       ],
                       "_909c5422c015cc9e3fbd3ba921f7a42fae089606" => [
                         %{
                           type: "ExternalData",
                           service: "geoshape",
                           url: "https://maps.wikimedia.org/geoshape?getgeojson=1&ids=Q797"
                         }
                       ]
                     })
                   ]
                 }
               ]
             }
           },
           headers: [],
           status: 200
       }}
    end)

    # https://www.mediawiki.org/api/rest_v1/page/html/Help:Extension:Kartographer/5843699
    %{
      mapframe_count: 29,
      mapframe_from_transclusion_count: 0,
      maplink_count: 9,
      maplink_from_transclusion_count: 0,
      externaldata: %{"geopoint_ids" => 1, "geoshape_ids" => 1}
    }
    |> assert_analysis(
      Wiki.DumpsAnalysis.KartographerMaps,
      parse_html_file("./test/data/Help-Extension-Kartographer.html")
    )
  end

  test "aggregate" do
    expected = %{
      externaldata_geoline_ids: 0,
      externaldata_geoline_query: 0,
      externaldata_geomask_ids: 0,
      externaldata_geomask_query: 0,
      externaldata_geopoint_ids: 2,
      externaldata_geopoint_query: 0,
      externaldata_geoshape_ids: 0,
      externaldata_geoshape_query: 0,
      externaldata_page: 0,
      mapframe_from_transclusion_sum: 3,
      mapframe_sum: 4,
      maplink_from_transclusion_sum: 2,
      maplink_sum: 3,
      pages_with_maps_count: 2
    }

    actual =
      "./test/data/fixture5-summary.ndjson"
      |> DumpUtils.stream_binary_file()
      |> SplitLines.stream!()
      |> DumpUtils.decode_ndjson()
      |> SummaryAggregator.aggregate_stream([
        Wiki.DumpsAnalysis.KartographerMaps
      ])

    assert expected == actual
  end
end
