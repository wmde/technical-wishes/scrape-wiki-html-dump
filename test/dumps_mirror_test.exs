defmodule Wiki.DumpsMirrorTest do
  use ExUnit.Case, async: true

  test "constructs path as expected" do
    assert "#{System.tmp_dir!()}/dumps/20230102/testwiki-NS0-20230102-ENTERPRISE-HTML.json.tar.gz" ==
             Wiki.DumpsMirror.find_dump("testwiki", "20230102")
  end

  test "parses wiki from dump path" do
    wikis =
      ~w(
        README.md
        aawiktionary-NS0-20210102-ENTERPRISE-HTML.json.tar.gz
        testwiki-NS0-20220203-ENTERPRISE-HTML.json.tar.gz
        commons-NS0-20230405-ENTERPRISE-HTML.json.tar.gz
      )
      |> Wiki.DumpsMirror.dump_paths_to_wikis()

    assert wikis == ~w(aawiktionary testwiki commons)
  end
end
