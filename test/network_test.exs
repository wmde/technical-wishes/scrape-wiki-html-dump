defmodule Wiki.NetworkTest do
  use ExUnit.Case
  import Mox

  test "read entire stream" do
    Wiki.NetworkTest.MockHttpStreamAdapter
    |> expect(:request, fn %HTTPStream.Request{} = request ->
      assert request.host == "wiki.test"
      assert request.path == "/foo"
      assert request.headers |> hd() |> elem(0) == "user-agent"
      :first
    end)
    |> expect(:parse_chunks, fn :first -> {["abc"], :second} end)
    |> expect(:parse_chunks, fn :second -> {:halt, :third} end)
    |> expect(:close, fn _ -> :ok end)

    assert Wiki.Network.read_from_url("http://wiki.test/foo") == "abc"
  end
end
