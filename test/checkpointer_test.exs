defmodule Wiki.CheckpointerTest do
  use ExUnit.Case
  alias Wiki.Checkpointer

  import ExUnit.CaptureLog

  defp tmp_path do
    [
      System.tmp_dir!(),
      "#{__MODULE__}-output-#{:rand.uniform()}"
    ]
    |> Path.join()
  end

  test "resumable_pipeline normal fresh start" do
    path = tmp_path()

    Checkpointer.resumable_pipeline(
      path,
      fn -> 1..11 |> Stream.map(&to_string/1) end,
      fn x -> x end,
      interval: 10
    )

    assert "1234567891011" == File.read!(path)
  end

  test "skip when resuming" do
    path = tmp_path()

    assert_raise RuntimeError, "Stopped at 11", fn ->
      Checkpointer.resumable_pipeline(
        path,
        fn -> 1..20 |> Stream.map(&to_string/1) end,
        fn stream ->
          stream
          |> Stream.map(fn x ->
            if String.to_integer(x) > 10, do: raise("Stopped at #{x}"), else: x <> " "
          end)
        end,
        interval: 10
      )
    end

    assert File.exists?(checkpoint_path(path))

    logs =
      capture_log(fn ->
        Checkpointer.resumable_pipeline(
          path,
          fn -> 20..30 |> Stream.map(&to_string/1) end,
          fn stream ->
            stream
            |> Stream.map(fn x ->
              if String.to_integer(x) < 30, do: flunk(), else: x
            end)
          end
        )
      end)

    assert logs =~ "Resuming from checkpoint at 10 lines"

    assert "1 2 3 4 5 6 7 8 9 10 30" == File.read!(path)
    File.rm!(path)
  end

  test "resumable_pipeline skips when complete" do
    path = tmp_path()
    File.touch!(path)

    logs =
      capture_log(fn ->
        Checkpointer.resumable_pipeline(
          path,
          fn -> flunk() end,
          fn -> flunk() end
        )
      end)

    assert logs =~ "Skipping over complete"
  end

  test "run_once skips when complete" do
    path = tmp_path()
    File.touch!(path)

    logs =
      capture_log(fn ->
        Checkpointer.run_once(
          path,
          fn -> flunk() end,
          fn -> flunk() end
        )
      end)

    assert logs =~ "Skipping over complete"
  end

  test "run_once lockfile remains after crash" do
    path = tmp_path()

    assert_raise RuntimeError, "foo", fn ->
      Checkpointer.run_once(
        path,
        fn -> raise("foo") end,
        fn -> flunk() end
      )
    end

    assert not File.exists?(path)

    lockfile = checkpoint_path(path)
    assert File.exists?(lockfile)
    File.rm!(lockfile)
  end

  test "run_once repeats work after crash" do
    path = tmp_path()
    lockfile = checkpoint_path(path)
    File.touch!(path)
    File.write!(lockfile, "10")

    logs =
      capture_log(fn ->
        Checkpointer.run_once(
          path,
          fn -> 2 end,
          fn x -> to_string(x + 1) end
        )
      end)

    assert logs =~ "Overwriting incomplete output"

    assert "3" == File.read!(path)
    File.rm!(path)

    assert not File.exists?(lockfile)
  end

  # This is copied from the checkpointer, to avoid exposing as public interface.
  defp checkpoint_path(path), do: "#{Path.dirname(path)}/.lock.#{Path.basename(path)}"
end
