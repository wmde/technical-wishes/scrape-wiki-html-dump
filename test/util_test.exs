defmodule Wiki.DumpUtilsTest do
  use ExUnit.Case, async: true

  test "merge numeric maps" do
    assert %{a: 3, b: 1} == Wiki.DumpUtils.sum_maps(%{a: 1}, %{a: 2, b: 1})
  end

  test "merge malformed maps" do
    assert_raise FunctionClauseError, fn ->
      Wiki.DumpUtils.sum_maps(%{a: %{thing: 1}}, %{a: 2, b: 1})
    end
  end

  test "normal ratio" do
    assert 1.6667 == Wiki.DumpUtils.ratio(5, 3)
  end

  test "ratio divides by zero" do
    assert 0 == Wiki.DumpUtils.ratio(5, 0)
    assert 0 == Wiki.DumpUtils.ratio(0, 0)
  end
end
