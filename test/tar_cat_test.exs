defmodule TarCatTest do
  use ExUnit.Case, async: true

  test "read compressed tarball" do
    # Chop the test file into a stream of chunks to make it more fun.
    result =
      File.read!("./test/data/fixture3.tar.gz")
      |> :binary.bin_to_list()
      # Arbitrary chunk size
      |> Stream.chunk_every(23)
      |> TarCat.stream!()
      |> Enum.to_list()

    assert ["Line from a\nLine from b\n"] == result
  end
end
