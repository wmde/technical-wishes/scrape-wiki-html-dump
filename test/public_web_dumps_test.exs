defmodule Wiki.PublicWebDumpsTest do
  use ExUnit.Case, async: true

  test "parse snapshot index" do
    result =
      "./test/data/other_enterprise_html_runs_index.html"
      |> File.read!()
      |> Wiki.PublicWebDumps.parse_html_top_snapshot()

    assert "20231101" == result
  end

  test "dump url" do
    assert "https://dumps.wikimedia.org/other/enterprise_html/runs/20230101/testwiki-NS0-20230101-ENTERPRISE-HTML.json.tar.gz" ==
             Wiki.PublicWebDumps.dump_url_for_snapshot("testwiki", "20230101")
  end
end
