defmodule Wiki.TestUtils do
  import ExUnit.Assertions

  def parse_html_file(path) do
    path
    |> File.read!()
    |> Floki.parse_document!(html_parser: Floki.HTMLParser.FastHtml)
  end

  def parse_dump_file(path) do
    # FIXME: Replace dump fixtures with bare HTML.
    path
    |> File.read!()
    |> JiffyEx.decode!(return_maps: true)
    |> then(& &1["article_body"]["html"])
    |> Floki.parse_document!(html_parser: Floki.HTMLParser.FastHtml)
  end

  def test_site do
    %Wiki.SiteMatrix.Spec{
      base_url: "https://wiki.test",
      dbname: "test",
      opts: [adapter: Wiki.Action.MockAdapter]
    }
  end

  def assert_analysis(expected, plugin, dom) do
    assert expected ==
             %{dom: dom, revid: 123, site: test_site()}
             |> plugin.analyze_page()
  end
end

ExUnit.start()

Mox.defmock(Wiki.NetworkTest.MockHttpStreamAdapter, for: HTTPStream.Adapter)
Mox.defmock(Wiki.DumpPipelineTest.MockAnalysis, for: Wiki.DumpsAnalysis.Plugin)
Mox.defmock(Wiki.Action.MockAdapter, for: Tesla.Adapter)
