defmodule Wiki.MapdataTest do
  use ExUnit.Case, async: true
  import ExUnit.CaptureLog
  import Mox
  import Wiki.TestUtils

  test "analyze mapdata for Maptests" do
    # curl 'https://en.wikipedia.beta.wmflabs.org/w/api.php?action=query&prop=mapdata&revids=572894&formatversion=2&format=json' -O - | jq '.query.pages[0].mapdata[0] | fromjson' > test/data/fixture6.json
    expected = %{
      externaldata: %{
        "geoline_ids" => 1,
        "geoline_query" => 2,
        "geopoint_ids" => 1,
        "geopoint_query" => 2,
        "geoshape_ids" => 1,
        "geoshape_query" => 1
      }
    }

    actual =
      "./test/data/fixture6.json"
      |> File.read!()
      |> JiffyEx.decode!(return_maps: true)
      |> Wiki.Mapdata.analyze_all_mapdata()

    assert expected == actual
  end

  test "analyze mapdata for Help:Extension:Kartographer" do
    # curl 'https://www.mediawiki.org/w/api.php?action=query&prop=mapdata&revids=5843699&formatversion=2&format=json' | jq '.query.pages[0].mapdata[0] | fromjson' > test/data/fixture7.json
    expected = %{
      externaldata: %{
        "geoline_ids" => 1,
        "geopoint_ids" => 1,
        "geopoint_query" => 1,
        "geoshape_ids" => 5,
        "geoshape_query" => 2,
        "geomask_ids" => 1,
        "page" => 2
      }
    }

    actual =
      "./test/data/fixture7.json"
      |> File.read!()
      |> JiffyEx.decode!(return_maps: true)
      |> Wiki.Mapdata.analyze_all_mapdata()

    assert expected == actual
  end

  test "server error while fetching mapdata" do
    Wiki.Action.MockAdapter
    |> expect(:call, fn env, _opts ->
      {:ok, %Tesla.Env{env | body: %{}, headers: [], status: 500}}
    end)

    logs =
      capture_log(fn ->
        assert %{} == Wiki.Mapdata.fetch_and_summarize_mapdata(test_site(), 123)
      end)

    assert logs =~ "Error received with HTTP status 500"
  end

  test "missing mapdata" do
    Wiki.Action.MockAdapter
    |> expect(:call, fn env, _opts ->
      {:ok,
       %Tesla.Env{
         env
         | body: %{
             "batchcomplete" => true,
             "query" => %{
               "pages" => []
             }
           },
           headers: [],
           status: 200
       }}
    end)

    logs =
      capture_log(fn ->
        assert %{} == Wiki.Mapdata.fetch_and_summarize_mapdata(test_site(), 123)
      end)

    assert logs =~ "missing_page"
  end
end
