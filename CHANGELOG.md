# Change log

## [Unreleased]

...

## [0.3.1] 2024-05

### Added

- New output file `all-wikis-*-cite-error-summary.csv` compares error counts across wikis.
- Advanced code linting under `mix test.all`
- Explore some aggregate statistics using 25 refs as a segmentation threshold:
  - identical_refs_on_pages_with_25_or_less_refs_average
  - identical_refs_on_pages_with_over_25_refs_average
  - identical_refs_on_pages_with_over_25_refs_count
  - pages_with_identical_refs_and_over_25_refs_count
  - pages_with_over_25_refs_count

### Changed

- Fix handling of templates that emit multiple DOM elements.
  
  Invalidates the following columns from version 0.3:
  - potential_ref_transclusions
  - proportion_of_refs_from_transclusion
  - ref_by_transclusion_average
  - ref_by_transclusion_count
  - refs_with_solely_transclusion_count

### Removed

- Disable prose size analysis by default, for performance reasons.

## [0.3] 2024-04

### Added

- New page analysis fields:
  - nested_ref_count
  - potential_transclusions_with_top_level_refs
  - ref_by_top_ref_transclusion_count
  - refs_with_solely_transclusion_count
- New aggregate fields:
  - nested_ref_sum
  - pages_with_automatically_named_refs_count
  - pages_with_nested_refs_count
  - potential_transclusions_with_top_level_refs
  - proportion_of_pages_with_identical_refs
  - proportion_of_pages_with_nested_refs
  - proportion_of_pages_with_similar_refs
- Emits prometheus-compatible performance metrics at runtime.
- article_count_stats is a checksum coming directly from each wiki's API.

### Changed

- Use Erlang loggers and drop deprecated logger backend integration.
- Map data analysis is now disabled by default.
- Analysis plugins are more isolated and structured, and are no longer responsible for merging results from the other plugins.  The common aggregations can be expressed concisely.
- Let Flow choose the optimal concurrency.

### Removed

- `private` column flag was never implemented.

## [0.2] - 2023-09

### Changed

- Parallelize processing of pages within a wiki, but serialize processing of each wiki.
- Aggregation rejects duplicates by revid.
- "similar" and "identical" metrics now count refs and not congruencies.
- Explicitly list aggregate columns.
- Extract mapdata helpers into a separate code module.

## [0.1] - 2023-07

_Initial code and data release._

[Unreleased]: https://gitlab.com/wmde/technical-wishes/scrape-wiki-html-dump/-/compare/v0.3.1...HEAD
[0.3.1]: https://gitlab.com/wmde/technical-wishes/scrape-wiki-html-dump/-/compare/v0.3...v0.3.1
[0.3]: https://gitlab.com/wmde/technical-wishes/scrape-wiki-html-dump/-/compare/v0.2...v0.3
[0.2]: https://gitlab.com/wmde/technical-wishes/scrape-wiki-html-dump/-/compare/v0.1...v0.2
[0.1]: https://gitlab.com/wmde/technical-wishes/scrape-wiki-html-dump/-/tags/v0.1
