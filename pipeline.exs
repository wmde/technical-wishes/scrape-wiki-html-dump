defmodule Wiki.PipelineScript do
  @moduledoc """
  Automated processing pipeline: do all the things.

  Usage:
    mix run pipeline.exs dewikivoyage fawiki

    mix run pipeline.exs
  """
  alias Wiki.PublicWebDumps

  def main(args) do
    OptionParser.parse(args,
      strict: [
        fetch: :boolean,
        skip_page_summary: :boolean
      ]
    )
    |> case do
      {opts, wikis, []} ->
        snapshot =
          case Application.get_env(:scrape_wiki_dump, :snapshot) do
            :latest ->
              if opts[:fetch] != true do
                IO.puts(
                  :standard_error,
                  "Must use the --fetch option when processing the latest snapshot."
                )

                exit(1)
              end

              PublicWebDumps.find_latest_snapshot_date()

            x ->
              x
          end

        if opts[:fetch] do
          # TODO: should validate wiki list first
          PublicWebDumps.fetch_missing_dumps(wikis, snapshot)
        end

        Wiki.DumpPipeline.run_pipeline(wikis, snapshot, opts)

      _ ->
        IO.puts(:standard_error, @moduledoc)
        exit(1)
    end
  end
end

Wiki.PipelineScript.main(System.argv())
